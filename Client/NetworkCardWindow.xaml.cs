﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
//using static Vibecode_Helpdesk.UdpConnection;

namespace Vibecode_Helpdesk
{
    /// <summary>
    /// Logica di interazione per NetworkCardWindow.xaml
    /// </summary>
    public partial class NetworkCardWindow : Window
    {
        public LocalNetworkCards[] Items { get; set; }

        public LocalNetworkCards Selected { get; private set; }  = null;

        public NetworkCardWindow(LocalNetworkCards[] items)
        {
            Height = 48 + items.Count() * 26;

            InitializeComponent();
            Items = items;

            DataContext = this;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Selected = (sender as Button).DataContext as LocalNetworkCards;
            Close();
        }

        private void ListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            foreach (var item in e.AddedItems.OfType<LocalNetworkCards>())
            {
                Selected = item;
                Close();
                return;
            }
        }
    }
}
