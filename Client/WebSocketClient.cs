﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;
using WebSocketSharper;

namespace Vibecode_Helpdesk
{
    public class VibecodeHelpdeskP2PInfo
    {
        UInt32 id;
        UInt32 p2pId;
        public UInt32 Id => id;
        public UInt32 P2PId => p2pId;
        public string Id_ToString => $"Id: {id} ({ConnectedBy})";
        public string FullName { get; }
        public BitmapImage Avatar { get; }
        public string ConnectedBy { get; set; }
        public VibecodeHelpdeskP2PInfo(UInt32 id, UInt32 p2pId, string fullName, BitmapImage avatar)
        {
            this.id = id;
            this.p2pId = p2pId;
            FullName = fullName;
            Avatar = avatar;
        }
    }
    public class StreamPacket
    {
        public byte[] Buffer { get; set; }
        public int PacketLength { get; set; }
        public int PacketsCount { get; set; }
        public int PacketsReceived { get; set; }
        public string FileName { get; set; }
        public Stream Stream { get; set; }
    }

    public class LocalNetworkCards
    {
        public string Name { get; set; }
        public IPAddress Address { get; set; }
    }

    public class WebSocketClient : IDisposable
    {
        ILoggerFactory loggerFactory;
        WebSocket ws;

        public string BasePath { get; set; }

        public event EventHandler OnConnected;
        public event EventHandler OnDisconnected;
        public event EventHandler<vhpSubscribe> OnSubscribe;
        public event EventHandler<vhpSubscribeP2P> OnPeer2Peer;
        public event EventHandler<vhpCommand> OnCommand;
        public event EventHandler<vhpError> OnError;
        public event EventHandler<bool> OnBlinking;
        public event EventHandler<string> OnOuputLine;
        public event EventHandler<StreamPacket> OnDownload;
        public event EventHandler<int> OnDownloadProgress;
        public event EventHandler<vhpChat> OnChat;
        public static LocalNetworkCards[] GetLocalIp()
        {
            var networkCards = NetworkInterface.GetAllNetworkInterfaces().Where(i => i.OperationalStatus == OperationalStatus.Up && i.GetIPProperties().GatewayAddresses.Any(j => !string.IsNullOrEmpty(j.Address?.ToString()))).ToList();
            return networkCards.Select(j => new LocalNetworkCards { Name = j.Name, Address = j.GetIPProperties().UnicastAddresses.Where(i => i.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork).FirstOrDefault()?.Address }).ToArray();
        }

        public vhpSubscribe[] Sessions { get; private set; } = { null, null, null, null, null, null, null, null, null, null };

        public WebSocketClient(string serverIp, int port = 80)        
        {
            loggerFactory = LoggerFactory.Create(builder => builder.AddConsole());
            ILogger logger = loggerFactory.CreateLogger<App>();

            ws = new WebSocket(logger, $"ws://{serverIp}:{port}/VibecodeHelpdesk", true)
            {
                EmitOnPing = true,
                ReconnectDelay = new TimeSpan(0, 0, 10)
            };

            ws.OnOpen += OnOpen;
            ws.OnMessage += OnMessage;
            ws.OnClose += OnClose;
        }

        public bool IsAlive => ws.IsAlive;

        private void OnClose(object sender, CloseEventArgs e)
        {
            OnDisconnected?.Invoke(this, EventArgs.Empty);
        }

        private void OnOpen(object sender, EventArgs e)
        {
            OnConnected?.Invoke(this, EventArgs.Empty);

            if (ws.IsAlive)
            {
                if (!Sessions.Any(i => i != null))
                {
                    if (GetFreeSession() is vhpSubscribe subscribe)
                    {
                        ws.Send(subscribe.ToArray());
                    }
                    else
                        MessageBox.Show("Non ci sono sessioni libere; chiudere una sessione e riprovare.");
                }
            }
        }

        IPAddress LocalIP = null;
        int LocalUdpPort = 0;

        public void Start()
        {
            if (LocalIP == null)
            {
                LocalUdpPort = 0; // new Random().Next(10, 20) * 1000;

                var ip_address = GetLocalIp();
                switch (ip_address.Count())
                {
                    case 0:
                        break;
                    case 1:
                        LocalIP = ip_address.First().Address;
                        break;
                    default:
                        Application.Current.Dispatcher.Invoke((Action)delegate
                        {
                            NetworkCardWindow modalWindow = new NetworkCardWindow(ip_address);
                            modalWindow.ShowDialog();

                            if (modalWindow.Selected != null)
                                LocalIP = modalWindow.Selected.Address;
                        });
                        break;
                }
            }

            if (LocalIP != null)
                ws?.Connect();
        }

        void ClearSession(vhpSubscribe subscribe)
        {
            for (int i = 0; i < Sessions.Length; i++)
            {
                if (Sessions[i] == subscribe)
                {
                    if (subscribe.Session != 1)
                        Sessions[i] = null;

                    break;
                }
            }
        }
        vhpSubscribe GetFreeSession()
        {
            if (!Sessions.Any(i => i == null))
                return null;

            byte currentSession = (byte)(Sessions.Max(i => i?.Session ?? 0) + 1);

            for (int i = 0; i < Sessions.Length; i++)
            {
                if (Sessions[i] == null)
                {
                    Sessions[i] = new vhpSubscribe(LocalIP, currentSession, App.UserName, App.Avatar);
                    return Sessions[i];
                }
            }

            return null;
        }


        public void Send(byte[] data)
        {
            try
            {
                ws.Send(data);
            }
            catch(Exception)
            {

            }
        }

        public async Task SendUdp(byte session, byte[] data)
        {
            try
            {
                var id = Sessions.Where(i => i.Session == session).Select(i => i.Id).FirstOrDefault();
                if (id != default(UInt32))
                {
                    foreach (var udp in UdpClients.Values.Where(i => i.Id == id).ToList())
                        await udp.SendMessage(data);
                }
            }
            catch(Exception)
            {

            }
        }

        ConcurrentDictionary<uint, UdpSocketClient> UdpClients = new ConcurrentDictionary<uint, UdpSocketClient>();
        public static Dictionary<byte, StreamPacket> Packets = new Dictionary<byte, StreamPacket>();

        private void OnMessage(object sender, MessageEventArgs e)
        {
            if (e.IsBinary)
            {
                if (e.RawData.Length < sizeof(int))
                {
                    Console.WriteLine("Bad Message");
                    return;
                }

                switch ((VibecodeHelpdekProtocolEnum)BitConverter.ToInt32(e.RawData, 0))
                {
                    case VibecodeHelpdekProtocolEnum.Acknowledgement:
                        Console.WriteLine($"Acknowledgement");
                        return;

                    case VibecodeHelpdekProtocolEnum.Command:
                        if (vhpCommand.Parse(e.RawData) is vhpCommand command)
                        {
                            OnCommand?.Invoke(this, command);
                            return;
                        }
                        else
                            Console.WriteLine($"ERROR [Command]: Syntax error");
                        return;

                    case VibecodeHelpdekProtocolEnum.OutputLine:
                        if (vhpOutputLine.Parse(e.RawData) is vhpOutputLine ouputLine)
                            OnOuputLine?.Invoke(this, ouputLine.Line);
                        else
                            Console.WriteLine($"ERROR [OutputLine]: Syntax error");
                        return;

                    case VibecodeHelpdekProtocolEnum.Error:
                        if (vhpError.Parse(e.RawData) is vhpError error)
                        {
                            Console.WriteLine($"Error [{error.Code}]: {error.Message}");
                            OnError?.Invoke(this, error);
                            return;
                        }
                        else
                            Console.WriteLine($"ERROR [Error]: Syntax error");
                        return;

                    case VibecodeHelpdekProtocolEnum.Subscribe:
                        if (vhpSubscribe.Parse(e.RawData) is vhpSubscribe subscribe)
                        {
                            Console.WriteLine($"Subscribe [{subscribe.Session}]: {subscribe.Id}");
                            var obj = Sessions.Where(i => i.Session == subscribe.Session).FirstOrDefault();
                            if (obj != null)
                                obj.Id = subscribe.Id;

                            OnSubscribe?.Invoke(this, subscribe);
                            return;
                        }
                        else
                            Console.WriteLine($"ERROR [Subscribe]: Syntax error");
                        return;

                    case VibecodeHelpdekProtocolEnum.Disconnect:
                        if (vhpDisconnect.Parse(e.RawData) is vhpDisconnect disconnect)
                        {
                            Console.WriteLine($"Disconnect P2PId: {disconnect.P2PId}");
                            StopUdp(disconnect.P2PId);
                            return;
                        }
                        else
                            Console.WriteLine($"ERROR [Disconnect]: Syntax error");
                        return;

                    case VibecodeHelpdekProtocolEnum.Peer2Peer:
                        if (vhpPeer2Peer.Parse(e.RawData) is vhpPeer2Peer peer2peer)
                        {
                            Console.WriteLine($"Peer2Peer P2PId: {peer2peer.P2PId} - ServerUDP: {peer2peer.ServerUdpEndPoint.Address} {peer2peer.ServerUdpEndPoint.Port}");
                            StartUdp(peer2peer);
                            return;
                        }
                        else
                            Console.WriteLine($"ERROR [Peer2Peer]: Syntax error");
                        return;


                    case VibecodeHelpdekProtocolEnum.Stream:
                        if (vhpStream.Parse(e.RawData) is vhpStream stream)
                        {
                            if (stream.Buffer == null)
                            {
                                StreamPacket bufferClass;
                                lock (Packets)
                                {
                                    if (!Packets.TryGetValue(stream.Id, out bufferClass))
                                    {
                                        var fileName = Path.Combine(BasePath, stream.FileName);

                                        int i = 0;
                                        while (File.Exists(fileName))
                                            fileName = Path.Combine(BasePath, $"{Path.GetFileNameWithoutExtension(stream.FileName)} ({++i}){Path.GetExtension(stream.FileName)}");

                                        if (File.Exists($"{fileName}.partial"))
                                            File.Delete($"{fileName}.partial");

                                        Packets.Add(stream.Id, bufferClass = new StreamPacket
                                        {
                                            Stream = new FileStream($"{fileName}.partial", FileMode.CreateNew),
                                            PacketLength = stream.PacketLength,
                                            PacketsReceived = 0,
                                            PacketsCount = (int)Math.Ceiling((double)stream.Length / (double)stream.PacketLength),
                                            FileName = $"{fileName}.partial"
                                        });

                                        OnDownloadProgress?.Invoke(this, -bufferClass.PacketsCount);
                                    }
                                }
                            }
                            else
                            {
                                OnDownloadProgress?.Invoke(this, -1);

                                MemoryStream input = new MemoryStream(stream.Buffer);
                                MemoryStream output = new MemoryStream();
                                using (GZipStream dstream = new GZipStream(input, CompressionMode.Decompress))
                                {
                                    dstream.CopyTo(output);

                                    OnDownloadProgress?.Invoke(this, 1);

                                    OnDownload?.Invoke(this, new StreamPacket()
                                    {
                                        Buffer = output.ToArray(),
                                        FileName = stream.FileName
                                    });

                                    OnDownloadProgress?.Invoke(this, 0);
                                }
                            }
                        }
                        else
                            Console.WriteLine($"ERROR [Stream]: Syntax error");
                        return;

                    case VibecodeHelpdekProtocolEnum.StreamPacket:
                        if (vhpStreamPacket.Parse(e.RawData) is vhpStreamPacket packet)
                        {
                            StreamPacket bufferClass;
                            lock (Packets)
                            {
                                if (Packets.TryGetValue(packet.Id, out bufferClass))
                                {
                                    bufferClass.PacketsReceived++;
                                    bufferClass.Stream.Write(packet.Buffer, 0, packet.Buffer.Length);

                                    OnDownloadProgress?.Invoke(this, bufferClass.PacketsReceived);

                                    if (bufferClass.PacketsReceived == bufferClass.PacketsCount)
                                    {
                                        bufferClass.Stream.Flush();
                                        bufferClass.Stream.Close();
                                        bufferClass.Stream.Dispose();

                                        FileInfo fileToDecompress = new FileInfo(bufferClass.FileName);

                                        using (FileStream originalFileStream = fileToDecompress.OpenRead())
                                        {
                                            string currentFileName = fileToDecompress.FullName;
                                            string newFileName = currentFileName.Remove(currentFileName.Length - fileToDecompress.Extension.Length);

                                            using (FileStream decompressedFileStream = File.Create(newFileName))
                                            {
                                                using (GZipStream decompressionStream = new GZipStream(originalFileStream, CompressionMode.Decompress))
                                                {
                                                    decompressionStream.CopyTo(decompressedFileStream);
                                                }
                                            }
                                        }
                                        
                                        fileToDecompress.Delete();
                                        OnDownloadProgress?.Invoke(this, 0);

                                        lock (Packets)
                                        {
                                            Packets.Remove(packet.Id);
                                        }
                                    }

                                }
                            }
                        }
                        else
                            Console.WriteLine($"ERROR [StreamPacket]: Syntax error");
                        return;

                    case VibecodeHelpdekProtocolEnum.StreamPacketUdp:
                        if (vhpStreamPacketUdp.Parse(e.RawData) is vhpStreamPacketUdp packetUdp)
                        {
                            StreamPacket bufferClass;
                            lock (Packets)
                            {
                                if (!Packets.TryGetValue(packetUdp.Id, out bufferClass))
                                {
                                    Packets.Add(packetUdp.Id, bufferClass = new StreamPacket
                                    {
                                        Buffer = new byte[packetUdp.Length],
                                        PacketLength = packetUdp.Buffer.Length,
                                        PacketsReceived = 0,
                                        PacketsCount = (int)Math.Ceiling((double)packetUdp.Length / (double)packetUdp.Buffer.Length)
                                    });
                                }
                            }
                            bufferClass.PacketsReceived++;
                            packetUdp.Buffer.CopyTo(bufferClass.Buffer, packetUdp.PacketNumber * bufferClass.PacketLength);

                            if (bufferClass.PacketsReceived == bufferClass.PacketsCount)
                            {
                                OnDownload?.Invoke(this, bufferClass);
                                lock (Packets)
                                {
                                    UdpSocketClient.Frame++;
                                    Packets.Remove(packetUdp.Id);
                                }
                            }
                        }
                        else
                            Console.WriteLine($"ERROR [StreamPacketUdp]: Syntax error");
                        return;

                    case VibecodeHelpdekProtocolEnum.Chat:
                        if (vhpChat.Parse(e.RawData) is vhpChat chat)
                        {
                            if (chat.Status == vhpChat.ChatStstusEnum.Sent)
                                chat.Sender = UdpClients.Values.ToList().Select(i => i.ConnectedInfo).ToArray().Where(i => i.Id == chat.SenderId).Select(i => i.FullName).FirstOrDefault();
                            else
                                chat.Sender = null;

                            OnChat?.Invoke(this, chat);
                        }
                        else
                            Console.WriteLine($"ERROR [Chat]: Syntax error");
                        return;
                }
                
                Console.WriteLine("Bad Message");
            }
            else if (e.IsText)
            {
                Console.WriteLine($"OnMessage: Text={e.Data}");
            }
            else if (e.IsPing)
            {
                Console.WriteLine($"OnMessage: Ping");
            }
        }

        public UInt32 GetP2PId(UInt32 id)
        {
            var obj = UdpClients.Values.ToList().Where(i => i.Id == id).FirstOrDefault();
            if (obj != null)
            {
                return obj.P2PId;
            }
            return 0;
        }

        public VibecodeHelpdeskP2PInfo GetConnectedInfo(UInt32 id)
        {
            var obj = UdpClients.Values.ToList().Where(i => i.Id == id).FirstOrDefault();
            if (obj != null)
            {
                return obj.ConnectedInfo;
            }
            return null;
        }

        void StartUdp(vhpPeer2Peer peer2peer)
        {
            var obj = Sessions.Where(i => i?.Id == peer2peer.Id).FirstOrDefault();
            if (obj != null)
            {
                IPEndPoint LocalUdpEndPoint = new IPEndPoint(LocalIP, LocalUdpPort /*+ (int)obj.Session*/);

                var udp = new UdpSocketClient(obj.Id, peer2peer.ConnectTo, peer2peer.P2PId, LocalUdpEndPoint, peer2peer.ServerUdpEndPoint)
                {
                    ConnectedInfo = new VibecodeHelpdeskP2PInfo(peer2peer.ConnectTo, peer2peer.P2PId, peer2peer.FullName, peer2peer.Avatar?.ToBitmapImage())
                };

                udp.OnPeer2Peer += Udp_OnPeer2Peer;
                udp.OnDownload += Udp_OnDownload;
                udp.OnBlinking += Udp_OnBlinking;
                udp.OnOutputLine += Udp_OnOutputLine;
                
                udp.BasePath = BasePath;
                udp.Session = obj;

                UdpClients.AddOrUpdate(peer2peer.P2PId, udp, (key, oldValue) => udp);

                udp.Start();
            }
        }

        private void Udp_OnOutputLine(object sender, string e)
        {
            OnOuputLine?.Invoke(sender, e);
        }

        private void Udp_OnBlinking(object sender, bool e)
        {
            OnBlinking?.Invoke(sender, e);
        }

        public void StopUdp(UInt32 p2p)
        {
            if (UdpClients.TryRemove(p2p, out UdpSocketClient udp))
            {
                udp.OnPeer2Peer -= Udp_OnPeer2Peer;
                udp.OnDownload -= Udp_OnDownload;
                udp.OnBlinking -= Udp_OnBlinking;
                udp.OnOutputLine -= Udp_OnOutputLine;

                udp.Avatar = null;
                ClearSession(udp.Session);

                udp.Stop();
                Console.WriteLine($"UdpSocketClient {p2p} is disconnected");

                OnPeer2Peer?.Invoke(this, null);
            }
            else
                Console.WriteLine($"UdpSocketClient {p2p} not found");
        }

        public void Disconnect()
        {
            foreach (var p2p in UdpClients.Values.ToList())
                StopUdp(p2p.P2PId);

            var cmd = new vhpDisconnect();
            ws?.Send(cmd.ToArray());
        }

        private void Udp_OnDownload(object sender, StreamPacket e)
        {
            OnDownload?.Invoke(sender, e);
        }

        private void Udp_OnPeer2Peer(object sender, vhpSubscribeP2P e)
        {
            OnPeer2Peer?.Invoke(sender, e);
        }

        public void Dispose()
        {
            ws?.Close();
            ws = null;

            loggerFactory?.Dispose();
            loggerFactory = null;
        }
    }
}
