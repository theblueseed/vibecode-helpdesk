﻿using Syncfusion.Windows.PropertyGrid;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Vibecode_Helpdesk
{
    /// <summary>
    /// Logica di interazione per MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindowViewModel ViewModel { get; private set; }
        public PropertyGrid PropertyGrid => propertyGrid1;

        public MainWindow()
        {
            InitializeComponent();
            DataContext = ViewModel = new MainWindowViewModel(this);
        }

        public void OnConnected()
        {
            ((INotifyCollectionChanged)lstOutput.ItemsSource).CollectionChanged -= MainWindow_CollectionChanged;
            ((INotifyCollectionChanged)lstOutput.ItemsSource).CollectionChanged += MainWindow_CollectionChanged;
        }

        private void MainWindow_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add && ViewModel.Current.EnableAutoScroll)
            {
                if (lstOutput.Items.Count > 0)
                    lstOutput.ScrollIntoView(lstOutput.Items[lstOutput.Items.Count - 1]);
            }
        }

        private void propertyGrid1_AutoGeneratingPropertyGridItem(object sender, Syncfusion.Windows.PropertyGrid.AutoGeneratingPropertyGridItemEventArgs e)
        {
            e.ReadOnly = true;
        }

        private void propertyGrid1_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void propertyGrid1_SelectedPropertyItemChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {

        }

        private void treeView1_PreviewMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {

        }

        private void Ellipse_MouseUp(object sender, MouseButtonEventArgs e)
        {

        }

        private void btnConnect_Click(object sender, RoutedEventArgs e)
        {

        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (sender is TextBox textBox)
            {
                ecPeerIdPlaceholder.Visibility = string.IsNullOrEmpty(textBox.Text) ? Visibility.Visible : Visibility.Collapsed;
            }

            ViewModel.Current.ConnectCommand.RaiseCanExecuteChanged();
        }

        private void TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                if (ViewModel.Current.ConnectCommand.CanExecute(null))
                    ViewModel.Current.ConnectCommand.Execute(null);
            }
        }
    }
}
