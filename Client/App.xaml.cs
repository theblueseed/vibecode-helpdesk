﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;

namespace Vibecode_Helpdesk
{
    /// <summary>
    /// Logica di interazione per App.xaml
    /// </summary>
    public partial class App : Application
    {
        [DllImport("shell32.dll", EntryPoint = "#261",
                   CharSet = CharSet.Unicode, PreserveSig = false)]

        public static extern void GetUserTilePath(
          string username,
          UInt32 whatever, // 0x80000000
          StringBuilder picpath, int maxLength);

        public static string GetUserTilePath(string username)
        {   // username: use null for current user
            var sb = new StringBuilder(1000);
            GetUserTilePath(username, 0x80000000, sb, sb.Capacity);
            return sb.ToString();
        }

        public static Image GetUserTile(string username)
        {
            return Image.FromFile(GetUserTilePath(username));
        }

        public static byte[] ImageToByteArray(System.Drawing.Image imageIn)
        {
            using (var ms = new MemoryStream())
            {
                imageIn.Save(ms, imageIn.RawFormat);
                return ms.ToArray();
            }
        }

        public static byte[] Avatar { get; private set; }
        public static string UserName { get; private set; }
        public static ManualResetEvent manualReset { get; private set; }
        public App()
        {
            Avatar = ImageToByteArray(GetUserTile(null));
            Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense("NTY2ODQ5QDMxMzkyZTM0MmUzMFlBLzViR0pJczgxT0hBMnROeFV2dUN1enJRQW01M2pyalZ2MDByWVZxUGM9");
            UserName = Environment.UserName;

            manualReset = new ManualResetEvent(false);

            Task.Run(() =>
            {
                try
                {
                    UserName = System.DirectoryServices.AccountManagement.UserPrincipal.Current.DisplayName;
                }
                catch { }

                manualReset.Set();
            });
        }
    }
}
