﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace Vibecode_Helpdesk
{
    public class UdpSocketClient
    {
        public bool IsRunning { get; private set; }
        public bool UseRedirect { get; private set; }
        private bool IsLocalP2P { get; set; }
        private bool IsPeered { get; set; }

        private UdpClient _udpPuncher;
        private UdpClient _udpClient;
        private UdpClient _extraUdpClient;
        private bool _extraUdpClientConnected;

        private IPEndPoint _serverEndPoint;
        private IPEndPoint _publicP2PEndPoint;
        private IPEndPoint _localP2PEndPoint;
        private IPEndPoint _remoteP2PEndPoint;

        private vhpSubscribeP2P _subscribeP2P;

        public event EventHandler<vhpSubscribeP2P> OnPeer2Peer;
        public event EventHandler<bool> OnBlinking;
        public event EventHandler<string> OnOutputLine;
        public event EventHandler<StreamPacket> OnDownload;
        public string BasePath { get; set; }
        public UInt32 Id { get; }
        public UInt32 P2PId { get; }
        public VibecodeHelpdeskP2PInfo ConnectedInfo { get; set; }
        public string FullName { get; set; }
        public byte[] Avatar { get; set; }
        public vhpSubscribe Session { get; set; }
        public UdpSocketClient(UInt32 id, UInt32 remoteId, UInt32 p2p, IPEndPoint localEndPoint, IPEndPoint serverEndPoint)
        {
            Id = id;
            P2PId = p2p;

            _udpPuncher = new UdpClient(); // this guy is just for punching
            _udpPuncher.ExclusiveAddressUse = false;
            _udpPuncher.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
            _udpPuncher.Client.Bind(localEndPoint);

            localEndPoint.Port = (_udpPuncher.Client.LocalEndPoint as IPEndPoint).Port;

            _serverEndPoint = serverEndPoint;
            _subscribeP2P = new vhpSubscribeP2P(id, localEndPoint, p2p, remoteId);

            _udpClient = new UdpClient(); // this will keep hole alive, and also can send data
            _extraUdpClient = new UdpClient(); // i think, this guy is the best option for sending data (explained below)

            InitUdpClients(new[] { _udpClient, _extraUdpClient }, localEndPoint);
        }

        public async void Stop()
        {
            IsRunning = false;

            _localP2PEndPoint = null;
            _publicP2PEndPoint = null;
            _serverEndPoint = null;
            _remoteP2PEndPoint = null;

            await Task.Delay(500);

            _udpPuncher?.Close();
            _udpPuncher?.Dispose();
            _udpPuncher = null;

            _udpClient?.Close();
            _udpClient?.Dispose();
            _udpClient = null;

            _extraUdpClient?.Close();
            _extraUdpClient?.Dispose();
            _extraUdpClient = null;
        }

        private void InitUdpClients(IEnumerable<UdpClient> clients, EndPoint localEndPoint)
        {
            // if you don't want to use explicit localPort, you should create here one more UdpClient (X) and send something to server (it will automatically bind X to free port). then bind all clients to this port and close X
            foreach (var udpClient in clients)
            {
                udpClient.ExclusiveAddressUse = false;
                udpClient.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
                udpClient.Client.Bind(localEndPoint);
            }
        }

        public void Start()
        {
            Task.Run(() => StartListener());
            Task.Run(() => Ping());
        }

        private async void Ping()
        {
            IsRunning = true;

            var delay = 200;
            var data = _subscribeP2P.ToArray();

            for (int i = 0; i < 50; i++)
            {
                if (_publicP2PEndPoint == null && _localP2PEndPoint == null)
                {
                    try
                    {
                        _udpPuncher.Send(data, data.Length, _serverEndPoint);
                        Console.WriteLine($" >>> Sent UDP to server [ {_serverEndPoint.Address} : {_serverEndPoint.Port} ]");
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                    break;

                await Task.Delay(delay);
            }

            await Task.Delay(500);

            if (_publicP2PEndPoint == null && _localP2PEndPoint == null)
            {
                Stop();

                UseRedirect = true;
                IsRunning = false;
                IsPeered = true;

                ConnectedInfo.ConnectedBy = "TCP";

                OnPeer2Peer?.Invoke(this, _subscribeP2P);
                return;
            }

            _udpPuncher?.Close();
            _udpPuncher = null;

            var ping = new vhpPingP2P(true, false);

            for (int i = 0; i < 50; i++)
            {
                if (_remoteP2PEndPoint == null)
                {
                    ping.IsPublic = 0x00;
                    data = ping.ToArray();
                    try
                    {
                        await _udpClient.SendAsync(data, data.Length, _publicP2PEndPoint);
                        Console.WriteLine($" >>> Sent UDP to peer.public [ {_publicP2PEndPoint.Address} : {_publicP2PEndPoint.Port} ]");
                    }
                    catch (Exception)
                    {

                    }

                    ping.IsPublic = 0x01;
                    data = ping.ToArray();
                    try
                    {
                        await _udpClient.SendAsync(data, data.Length, _localP2PEndPoint);
                        Console.WriteLine($" >>> Sent UDP to peer.local [ {_localP2PEndPoint.Address} : {_localP2PEndPoint.Port} ]");
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                    break;

                await Task.Delay(delay);
            }

            await Task.Delay(500);

            if (_remoteP2PEndPoint == null)
            {
                Stop();

                UseRedirect = true;
                IsRunning = false;
                IsPeered = true;

                ConnectedInfo.ConnectedBy = "TCP";

                OnPeer2Peer?.Invoke(this, _subscribeP2P);
                return;
            }

            _extraUdpClientConnected = true;
            _extraUdpClient.Connect(_remoteP2PEndPoint);

            IsPeered = true;
            OnPeer2Peer?.Invoke(this, _subscribeP2P);

            var messageToPeer = VibecodeHelpdeskProtocol_Helper.Ping;
            delay = 1000;

            while (IsRunning)
            {
                try
                {
                    await _extraUdpClient.SendAsync(messageToPeer, messageToPeer.Length);
                    Console.WriteLine($" >>> Sent UDP to peer.peer");
                }
                catch(Exception)
                {

                }

                await Task.Delay(delay);
            }
        }

        public async Task SendMessage(byte[] data)
        {
            if (!IsRunning || !IsPeered)
                return;

            try
            {
                if (UseRedirect)
                {
                    var redirect = new vhpRedirectUdp(_publicP2PEndPoint, data).ToArray();

                    await _udpClient.SendAsync(redirect, redirect.Length, _serverEndPoint);
                    Console.WriteLine($" >>> Sent UDP to redirect [ {_publicP2PEndPoint.Address} : {_publicP2PEndPoint.Port} ]");
                }
                else
                {
                    if (_extraUdpClientConnected)
                    {
                        _extraUdpClient.Send(data, data.Length);
                        Console.WriteLine($" >>> Sent UDP to peer.peer");
                    }
                    else
                    {
                        if (_publicP2PEndPoint != null && !IsLocalP2P)
                        {
                            await _udpClient.SendAsync(data, data.Length, _publicP2PEndPoint);
                            Console.WriteLine($" >>> Sent UDP to peer.public [ {_publicP2PEndPoint.Address} : {_publicP2PEndPoint.Port} ]");
                        }
                        if (_localP2PEndPoint != null && IsLocalP2P)
                        {
                            await _udpClient.SendAsync(data, data.Length, _localP2PEndPoint);
                            Console.WriteLine($" >>> Sent UDP to peer.local [ {_localP2PEndPoint.Address} : {_localP2PEndPoint.Port} ]");
                        }
                    }
                }
            }
            catch(Exception)
            {

            }
        }

        bool _blinking = false;

        private async void StartListener()
        {
            IsRunning = true;

            while (IsRunning)
            {
                try
                {
                    // also important thing!
                    // when you did not punched hole yet, you must listen incoming packets using "puncher" (later we will close it).
                    // where you already have p2p connection (and "puncher" closed), use "non-puncher"
                    UdpClient udpClient = _publicP2PEndPoint == null && _localP2PEndPoint == null ? _udpPuncher : _udpClient;

                    var receivedResults = await udpClient.ReceiveAsync();
#pragma warning disable CS4014 // Non è possibile attendere la chiamata, pertanto l'esecuzione del metodo corrente continuerà prima del completamento della chiamata
                    Task.Run(() =>
                    {
                        if (IsRunning)
                        {
                            if (!_blinking)
                            {
                                _blinking = true;
                                Task.Run(() =>
                                {
                                    OnBlinking?.Invoke(this, !UseRedirect);
                                    _blinking = false;
                                });
                            }

                            EngageMessage(receivedResults.Buffer, receivedResults.RemoteEndPoint);
                        }
                    });
#pragma warning restore CS4014 // Non è possibile attendere la chiamata, pertanto l'esecuzione del metodo corrente continuerà prima del completamento della chiamata
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Error: {ex.Message}");
                }
            }
        }

        public static Dictionary<byte, StreamPacket> Packets = new Dictionary<byte, StreamPacket>();
        public static int Frame;

        private async void EngageMessage(byte[] data, IPEndPoint remoteEndPoint)
        {
            if (data.Length < sizeof(int))
            {
                Console.WriteLine("Bad Message");
                return;
            }

            try
            {
                switch ((VibecodeHelpdekProtocolEnum)BitConverter.ToInt32(data, 0))
                {
                    case VibecodeHelpdekProtocolEnum.Acknowledgement:
                        Console.WriteLine("Acknowledgement");
                        return;

                    case VibecodeHelpdekProtocolEnum.PingP2P:
                        if (vhpPingP2P.Parse(data) is vhpPingP2P ping)
                        {
                            if (ping.IsMaster == 0x01)
                            {
                                if (ping.IsPublic == 0x01)
                                {
                                    Console.WriteLine($"UDP PingP2P IsPublic: [{remoteEndPoint.Address}:{remoteEndPoint.Port}]");
                                    _remoteP2PEndPoint = remoteEndPoint;
                                }
                                else
                                {
                                    Console.WriteLine($"UDP PingP2P IsLocal: [{remoteEndPoint.Address}:{remoteEndPoint.Port}]");
                                    _remoteP2PEndPoint = remoteEndPoint;
                                }
                            }
                            else
                            {
                                Console.WriteLine($"UDP PingP2P: [{remoteEndPoint.Address}:{remoteEndPoint.Port}]");
                                await _udpClient.SendAsync(data, data.Length, remoteEndPoint);
                            }
                        }
                        else
                            Console.WriteLine($"ERROR [UDP PingP2P]: Syntax error");
                        return;

                    case VibecodeHelpdekProtocolEnum.Ping:
                        Console.WriteLine($"Ping");
                        return;

                    case VibecodeHelpdekProtocolEnum.OutputLine:
                        if (vhpOutputLine.Parse(data) is vhpOutputLine ouputLine)
                            OnOutputLine?.Invoke(this, ouputLine.Line);
                        else
                            Console.WriteLine($"ERROR [UDP OutputLine]: Syntax error");
                        return;

                    case VibecodeHelpdekProtocolEnum.StreamPacketUdp:
                        if (vhpStreamPacketUdp.Parse(data) is vhpStreamPacketUdp packet)
                        {
                            StreamPacket bufferClass;
                            lock (Packets)
                            {
                                if (!Packets.TryGetValue(packet.Id, out bufferClass))
                                {
                                    Packets.Add(packet.Id, bufferClass = new StreamPacket
                                    {
                                        Buffer = new byte[packet.Length],
                                        PacketLength = packet.Buffer.Length,
                                        PacketsReceived = 0,
                                        PacketsCount = (int)Math.Ceiling((double)packet.Length / (double)packet.Buffer.Length)
                                    });
                                }
                            }
                            bufferClass.PacketsReceived++;
                            packet.Buffer.CopyTo(bufferClass.Buffer, packet.PacketNumber * bufferClass.PacketLength);

                            if (bufferClass.PacketsReceived == bufferClass.PacketsCount)
                            {
                                OnDownload?.Invoke(this, bufferClass);
                                lock (Packets)
                                {
                                    Frame++;
                                    Packets.Remove(packet.Id);
                                }
                            }
                        }
                        else
                            Console.WriteLine($"ERROR [UDP StreamPacket]: Syntax error");
                        return;

                    case VibecodeHelpdekProtocolEnum.SubscribeP2P:
                        if (vhpSubscribeP2P.Parse(data) is vhpSubscribeP2P subscribe)
                        {
                            Console.WriteLine($"UDP SubscribeP2P: [{remoteEndPoint.Address}:{remoteEndPoint.Port}]");
                            if (subscribe.PublicP2PEndPoint != null && subscribe.PublicP2PEndPoint.Port != 0)
                            {
                                _subscribeP2P = subscribe;
                                _localP2PEndPoint = subscribe.LocalP2PEndPoint;
                                _publicP2PEndPoint = subscribe.PublicP2PEndPoint;

                                IsLocalP2P = subscribe.IsLocalP2P;

                                ConnectedInfo.ConnectedBy = "P2P";
                            }
                        }
                        else
                            Console.WriteLine($"ERROR [UDP SubscribeP2P]: Syntax error");
                        return;
                }

                Console.WriteLine("Bad Message");
            }
            catch (Exception)
            {

            }
        }
    }
}
