﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Vibecode_Helpdesk
{
    public enum VibecodeHelpdekProtocolEnum { Unknown = 0, Acknowledgement = 0x50485600, Subscribe, Peer2Peer, Ping, PingP2P, Redirect, SubscribeP2P, Command, Stream, StreamPacket, StreamPacketUdp, Disconnect, Error, OutputLine, Chat };

    public static class VibecodeHelpdeskProtocol_Helper
    {
        public static BitmapImage ToBitmapImage(this byte[] buffer)
        {
            if (buffer != null)
            {
                using (var stream = new MemoryStream(buffer))
                {
                    var bitmap = new BitmapImage();
                    bitmap.BeginInit();
                    bitmap.StreamSource = stream;
                    bitmap.CacheOption = BitmapCacheOption.OnLoad;
                    try
                    {
                        bitmap.EndInit();
                        bitmap.Freeze();

                        return bitmap;
                    }
                    catch
                    {
                        return null;
                    }
                }
            }

            return null;
        }

        public static byte[] Acknowledgement = new vhpAcknowledgement().ToArray();
        public static byte[] Ping = new vhpPing().ToArray();

        public static byte[] Encrypt(this byte[] clearBytes)
        {
            string EncryptionKey = "VibecodeRetailAssistant2019";
            using (Aes encryptor = Aes.Create())
            {
                using (Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x43, 0x61, 0x72, 0x6c, 0x6f, 0x20, 0x47, 0x72, 0x69, 0x67, 0x69, 0x6f, 0x6e, 0x69 }))
                {
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(clearBytes, 0, clearBytes.Length);
                            cs.Close();
                        }

                        return ms.ToArray();
                    }
                }
            }
        }

        public static byte[] Decrypt(this byte[] cipherBytes, int offset = 0)
        {
            try
            {
                string EncryptionKey = "VibecodeRetailAssistant2019";
                using (Aes encryptor = Aes.Create())
                {
                    using (Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x43, 0x61, 0x72, 0x6c, 0x6f, 0x20, 0x47, 0x72, 0x69, 0x67, 0x69, 0x6f, 0x6e, 0x69 }))
                    {
                        encryptor.Key = pdb.GetBytes(32);
                        encryptor.IV = pdb.GetBytes(16);
                        using (MemoryStream ms = new MemoryStream())
                        {
                            using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                            {
                                cs.Write(cipherBytes, offset, cipherBytes.Length - offset);
                                cs.Close();
                            }

                            return ms.ToArray();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"VibecodeHelpers.Decrypt: Throw Exceptions {e.Message}");
            }

            return null;
        }
    }

    public abstract class VibecodeHelpdeskProtocol
    {
        public const int sizeof_IPAddress = 4;
        public const int sizeof_byte = 1;
        public const int sizeof_Guid = 16;
        public readonly byte[] IPAddress_Empty = { 0x00, 0x00, 0x00, 0x00 };

        public abstract byte[] ToArray();
    }
    public class vhpAcknowledgement : VibecodeHelpdeskProtocol
    {
        public override byte[] ToArray()
        {
            return BitConverter.GetBytes((Int32)VibecodeHelpdekProtocolEnum.Acknowledgement);
        }
    }
    public class vhpSubscribe : VibecodeHelpdeskProtocol
    {
        public IPAddress LocalIP { get; }
        public byte Session { get; }
        public string FullName { get; }
        public byte[] Avatar { get; }
        public UInt32 Id { get; set; }

        public vhpSubscribe(IPAddress address, byte session = 0, string fullName = null, byte[] avatar = null, UInt32 id = 0)
        {
            LocalIP = address;
            Session = session;
            FullName = fullName;
            Avatar = avatar;
            Id = id;
        }

        public override byte[] ToArray()
        {
            var encrypt_data = LocalIP.GetAddressBytes().
                               Concat(new byte[] { Session }).
                               Concat(BitConverter.GetBytes((Int32)Encoding.UTF8.GetByteCount(FullName ?? string.Empty))).
                               Concat(Encoding.UTF8.GetBytes(FullName ?? string.Empty)).
                               Concat(BitConverter.GetBytes((Int32)(Avatar?.Length ?? 0))).
                               Concat(Avatar ?? new byte[] { }).
                               Concat(BitConverter.GetBytes(Id)).ToArray().Encrypt();

            return BitConverter.GetBytes((Int32)VibecodeHelpdekProtocolEnum.Subscribe).Concat(encrypt_data).ToArray();
        }

        public static vhpSubscribe Parse(byte[] data)
        {
            try
            {
                var decript_data = data.Decrypt(sizeof(Int32));

                IPAddress localIp = new IPAddress((Int64)BitConverter.ToUInt32(decript_data, 0));
                byte session = decript_data[sizeof_IPAddress];
                int fullName_length = BitConverter.ToInt32(decript_data, sizeof_IPAddress + sizeof_byte);
                string fullName = null;
                if (fullName_length != 0)
                    fullName = Encoding.UTF8.GetString(decript_data, sizeof_IPAddress + sizeof_byte + sizeof(Int32), fullName_length);
                int avatar_length = BitConverter.ToInt32(decript_data, sizeof_IPAddress + sizeof_byte + sizeof(Int32) + fullName_length);
                byte[] avatar = null;
                if (avatar_length != 0)
                    avatar = new ArraySegment<byte>(decript_data, sizeof_IPAddress + sizeof_byte + sizeof(Int32) + fullName_length + sizeof(Int32), avatar_length).ToArray();
                UInt32 id = BitConverter.ToUInt32(decript_data, sizeof_IPAddress + sizeof_byte + sizeof(Int32) + fullName_length + sizeof(Int32) + avatar_length);

                return new vhpSubscribe(localIp, session, fullName, avatar, id);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
    public class vhpPeer2Peer : VibecodeHelpdeskProtocol
    {
        public UInt32 Id { get; set; }
        public UInt32 ConnectTo { get; private set; }
        public UInt32 P2PId { get; set; }
        public IPEndPoint ServerUdpEndPoint { get; set; }
        public string FullName { get; set; }
        public byte[] Avatar { get; set; }


        public vhpPeer2Peer(UInt32 id, UInt32 connectTo, UInt32 p2p = 0, IPEndPoint serverUdp = null, string fullName = null, byte[] avatar = null)
        {
            Id = id;
            ConnectTo = connectTo;
            P2PId = p2p;
            ServerUdpEndPoint = serverUdp;
            FullName = fullName;
            Avatar = avatar;
        }

        public override byte[] ToArray()
        {
            var encrypt_data = BitConverter.GetBytes(Id).
                               Concat(BitConverter.GetBytes(ConnectTo)).
                               Concat(BitConverter.GetBytes(P2PId)).
                               Concat(ServerUdpEndPoint?.Address.GetAddressBytes() ?? IPAddress_Empty).Concat(BitConverter.GetBytes((Int32)(ServerUdpEndPoint?.Port ?? 0))).
                               Concat(BitConverter.GetBytes((Int32)Encoding.UTF8.GetByteCount(FullName ?? string.Empty))).
                               Concat(Encoding.UTF8.GetBytes(FullName ?? string.Empty)).
                               Concat(BitConverter.GetBytes((Int32)(Avatar?.Length ?? 0))).
                               Concat(Avatar ?? new byte[] { }).
                               ToArray().Encrypt();

            return BitConverter.GetBytes((Int32)VibecodeHelpdekProtocolEnum.Peer2Peer).Concat(encrypt_data).ToArray();
        }

        public static vhpPeer2Peer Parse(byte[] data)
        {
            try
            {
                var decript_data = data.Decrypt(sizeof(Int32));

                UInt32 id = BitConverter.ToUInt32(decript_data, 0);
                UInt32 connectTo = BitConverter.ToUInt32(decript_data, sizeof(UInt32));
                UInt32 p2p = BitConverter.ToUInt32(decript_data, sizeof(UInt32) + sizeof(UInt32));

                IPAddress serverAddress = new IPAddress((Int64)BitConverter.ToUInt32(decript_data, sizeof(UInt32) + sizeof(UInt32) + sizeof(UInt32)));
                Int32 serverPort = BitConverter.ToInt32(decript_data, sizeof(UInt32) + sizeof(UInt32) + sizeof(UInt32) + sizeof_IPAddress);

                int fullName_length = BitConverter.ToInt32(decript_data, sizeof(UInt32) + sizeof(UInt32) + sizeof(UInt32) + sizeof_IPAddress + sizeof(Int32));
                string fullName = null;
                if (fullName_length != 0)
                    fullName = Encoding.UTF8.GetString(decript_data, sizeof(UInt32) + sizeof(UInt32) + sizeof(UInt32) + sizeof_IPAddress + sizeof(Int32) + sizeof(Int32), fullName_length);
                int avatar_length = BitConverter.ToInt32(decript_data, sizeof(UInt32) + sizeof(UInt32) + sizeof(UInt32) + sizeof_IPAddress + sizeof(Int32) + sizeof(Int32) + fullName_length);
                byte[] avatar = null;
                if (avatar_length != 0)
                    avatar = new ArraySegment<byte>(decript_data, sizeof(UInt32) + sizeof(UInt32) + sizeof(UInt32) + sizeof_IPAddress + sizeof(Int32) + sizeof(Int32) + fullName_length + sizeof(Int32), avatar_length).ToArray();

                return new vhpPeer2Peer(id, connectTo, p2p, new IPEndPoint(serverAddress, serverPort), fullName, avatar);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
    public class vhpPing : VibecodeHelpdeskProtocol
    {
        public override byte[] ToArray()
        {
            return BitConverter.GetBytes((Int32)VibecodeHelpdekProtocolEnum.Ping);
        }
    }
    public class vhpPingP2P : VibecodeHelpdeskProtocol
    {
        public byte IsMaster { get; }
        public byte IsPublic { get; set; }

        public vhpPingP2P(bool isMaster, bool isPublic = false)
        {
            IsMaster = isMaster ? (byte)0x01 : (byte)0x00;
            IsPublic = isPublic ? (byte)0x01 : (byte)0x00;
        }

        public override byte[] ToArray()
        {
            return BitConverter.GetBytes((Int32)VibecodeHelpdekProtocolEnum.PingP2P).
                   Concat(new byte[] { IsMaster, IsPublic }).ToArray();
        }

        public static vhpPingP2P Parse(byte[] data)
        {
            try
            {
                byte isMaster = data[sizeof(Int32)];
                byte isPublic = data[sizeof(Int32) + sizeof_byte];

                return new vhpPingP2P(isMaster == 0x01, isPublic == 0x01);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
    public class vhpRedirect : VibecodeHelpdeskProtocol
    {
        public UInt32 Id { get; private set; }
        public UInt32 P2PId { get; private set; }
        public byte[] Message { get; private set; }

        public vhpRedirect(UInt32 id, UInt32 p2p, byte[] message)
        {
            Id = id;
            P2PId = p2p;
            Message = message;
        }

        public override byte[] ToArray()
        {
            return BitConverter.GetBytes((Int32)VibecodeHelpdekProtocolEnum.Redirect).
                   Concat(BitConverter.GetBytes(Id)).
                   Concat(BitConverter.GetBytes(P2PId)).
                   Concat(Message).ToArray();
        }

        public static vhpRedirect Parse(byte[] data)
        {
            try
            {
                UInt32 id = BitConverter.ToUInt32(data, sizeof(Int32));
                UInt32 p2p = BitConverter.ToUInt32(data, sizeof(Int32) + sizeof(UInt32));
                byte[] message = new byte[] { };
                if (data.Length > sizeof(Int32) + sizeof(UInt32) + sizeof(UInt32))
                    message = data.Skip(sizeof(Int32) + sizeof(UInt32) + sizeof(UInt32)).ToArray();

                return new vhpRedirect(id, p2p, message);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
    public class vhpRedirectUdp : VibecodeHelpdeskProtocol
    {
        public IPEndPoint RedirectUdpEndPoint { get; private set; }
        public byte[] Message { get; private set; }

        public vhpRedirectUdp(IPEndPoint redirectUdp, byte[] message)
        {
            RedirectUdpEndPoint = redirectUdp;
            Message = message;
        }

        public override byte[] ToArray()
        {
            return BitConverter.GetBytes((Int32)VibecodeHelpdekProtocolEnum.Redirect).
                   Concat(RedirectUdpEndPoint?.Address.GetAddressBytes() ?? IPAddress_Empty).Concat(BitConverter.GetBytes((Int32)(RedirectUdpEndPoint?.Port ?? 0))).
                   Concat(Message).ToArray();
        }

        public static vhpRedirectUdp Parse(byte[] data)
        {
            try
            {
                IPAddress redirectAddress = new IPAddress((Int64)BitConverter.ToUInt32(data, sizeof(Int32)));
                Int32 redirectPort = BitConverter.ToInt32(data, sizeof(Int32) + sizeof_IPAddress);
                byte[] message = new byte[] { };
                if (data.Length > sizeof(Int32) + sizeof_IPAddress + sizeof(Int32))
                    message = data.Skip(sizeof(Int32) + sizeof_IPAddress + sizeof(Int32)).ToArray();

                return new vhpRedirectUdp(new IPEndPoint(redirectAddress, redirectPort), message);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
    public class vhpSubscribeP2P : VibecodeHelpdeskProtocol
    {
        public UInt32 Id { get; }
        public IPEndPoint LocalEndPoint { get; }
        public UInt32 P2PId { get; }
        public UInt32 RemoteId { get; set; }
        public IPEndPoint LocalP2PEndPoint { get; set; }
        public IPEndPoint PublicP2PEndPoint { get; set; }
        public bool IsLocalP2P { get; set; }

        public vhpSubscribeP2P(UInt32 id, IPEndPoint localEndPoint, UInt32 p2p, UInt32 remoteId = 0, IPEndPoint localP2PEndPoint = null, IPEndPoint publicP2PEndPoint = null, bool is_local = false)
        {
            Id = id;
            LocalEndPoint = localEndPoint;
            P2PId = p2p;
            RemoteId = remoteId;
            LocalP2PEndPoint = localP2PEndPoint;
            PublicP2PEndPoint = publicP2PEndPoint;
            IsLocalP2P = is_local;
        }

        public override byte[] ToArray()
        {
            var encrypt_data = BitConverter.GetBytes(Id).
                               Concat(LocalEndPoint?.Address.GetAddressBytes() ?? IPAddress_Empty).Concat(BitConverter.GetBytes((Int32)(LocalEndPoint?.Port ?? 0))).
                               Concat(BitConverter.GetBytes(P2PId)).
                               Concat(BitConverter.GetBytes(RemoteId)).
                               Concat(LocalP2PEndPoint?.Address.GetAddressBytes() ?? IPAddress_Empty).Concat(BitConverter.GetBytes((Int32)(LocalP2PEndPoint?.Port ?? 0))).
                               Concat(PublicP2PEndPoint?.Address.GetAddressBytes() ?? IPAddress_Empty).Concat(BitConverter.GetBytes((Int32)(PublicP2PEndPoint?.Port ?? 0))).
                               Concat(new byte[] { (byte)(IsLocalP2P ? 0x01 : 0x00) }).ToArray().Encrypt();

            return BitConverter.GetBytes((Int32)VibecodeHelpdekProtocolEnum.SubscribeP2P).Concat(encrypt_data).ToArray();
        }

        public static vhpSubscribeP2P Parse(byte[] data)
        {
            try
            {
                var decript_data = data.Decrypt(sizeof(Int32));

                UInt32 id = BitConverter.ToUInt32(decript_data, 0);
                IPAddress localIp = new IPAddress((Int64)BitConverter.ToUInt32(decript_data, sizeof(UInt32)));
                Int32 localPort = BitConverter.ToInt32(decript_data, sizeof(UInt32) + sizeof_IPAddress);
                UInt32 p2p = BitConverter.ToUInt32(decript_data, sizeof(UInt32) + sizeof_IPAddress + sizeof(Int32));
                UInt32 remoteId = BitConverter.ToUInt32(decript_data, sizeof(UInt32) + sizeof_IPAddress + sizeof(Int32) + sizeof(UInt32));
                IPAddress p2pLocalAddress = new IPAddress((Int64)BitConverter.ToUInt32(decript_data, sizeof(UInt32) + sizeof_IPAddress + sizeof(Int32) + sizeof(UInt32) + sizeof(UInt32)));
                Int32 p2pLocalPort = BitConverter.ToInt32(decript_data, sizeof(UInt32) + sizeof_IPAddress + sizeof(Int32) + sizeof(UInt32) + sizeof(UInt32) + sizeof_IPAddress);
                IPAddress p2pPublicAddress = new IPAddress((Int64)BitConverter.ToUInt32(decript_data, sizeof(UInt32) + sizeof_IPAddress + sizeof(Int32) + sizeof(UInt32) + sizeof(UInt32) + sizeof_IPAddress + sizeof(Int32)));
                Int32 p2pPublicPort = BitConverter.ToInt32(decript_data, sizeof(UInt32) + sizeof_IPAddress + sizeof(Int32) + sizeof(UInt32) + sizeof(UInt32) + sizeof_IPAddress + sizeof(Int32) + sizeof_IPAddress);

                bool isLocalP2P = decript_data[sizeof(UInt32) + sizeof_IPAddress + sizeof(Int32) + sizeof(UInt32) + sizeof(UInt32) + sizeof_IPAddress + sizeof(Int32) + sizeof_IPAddress + sizeof(Int32)] == 0x01;

                return new vhpSubscribeP2P(id, new IPEndPoint(localIp, localPort), p2p, remoteId, new IPEndPoint(p2pLocalAddress, p2pLocalPort), new IPEndPoint(p2pPublicAddress, p2pPublicPort), isLocalP2P);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
    public class vhpCommand : VibecodeHelpdeskProtocol
    {
        public enum vhpCommandEnum { ScreenCapture, Download, GetInfo }
        public UInt32 P2PId { get; }
        public Int32 Command { get; }
        public object[] Parameters { get; }
        public object Result { get; set; }
        public Int32 ErrorCode { get; }
        public string ErrorMessage { get; }

        public vhpCommand(UInt32 p2p, vhpCommandEnum command, object[] parameters, object result = null, Int32 error_code = 0, string error_message = null)
        {
            P2PId = p2p;
            Command = (Int32)command;
            Parameters = parameters;
            Result = result;
            ErrorCode = error_code;
            ErrorMessage = error_message;
        }

        static IEnumerable<byte> ObjectToArray(object obj)
        {
            if (obj == null)
                return new byte[] { 0 };
            if (obj is byte _byte)
                return new byte[] { 1 }.Concat(BitConverter.GetBytes(_byte));
            if (obj is byte[] _bytes)
                return new byte[] { 2 }.Concat(BitConverter.GetBytes((Int32)_bytes.Length)).Concat(_bytes);
            if (obj is Int16 _int16)
                return new byte[] { 3 }.Concat(BitConverter.GetBytes(_int16));
            if (obj is UInt16 _uint16)
                return new byte[] { 4 }.Concat(BitConverter.GetBytes(_uint16));
            if (obj is Int32 _int32)
                return new byte[] { 5 }.Concat(BitConverter.GetBytes(_int32));
            if (obj is UInt32 _uint32)
                return new byte[] { 6 }.Concat(BitConverter.GetBytes(_uint32));
            if (obj is Int64 _int64)
                return new byte[] { 7 }.Concat(BitConverter.GetBytes(_int64));
            if (obj is UInt64 _uint64)
                return new byte[] { 8 }.Concat(BitConverter.GetBytes(_uint64));
            if (obj is float _float)
                return new byte[] { 9 }.Concat(BitConverter.GetBytes(_float));
            if (obj is double _double)
                return new byte[] { 10 }.Concat(BitConverter.GetBytes(_double));
            if (obj is bool _bool)
                return new byte[] { 11 }.Concat(_bool ? new byte[] { 1 } : new byte[] { 0 });
            if (obj is string _string)
                return new byte[] { 12 }.Concat(BitConverter.GetBytes((Int32)Encoding.UTF8.GetByteCount(_string))).Concat(Encoding.UTF8.GetBytes(_string));
            if (obj is DateTime _dateTime)
                return new byte[] { 13 }.Concat(BitConverter.GetBytes((Int64)_dateTime.Ticks));
            if (obj is Guid _guid)
                return new byte[] { 14 }.Concat(_guid.ToByteArray());

            throw new Exception("Unmanaged type");
        }

        public override byte[] ToArray()
        {
            var encrypt_data = BitConverter.GetBytes(P2PId).
                               Concat(BitConverter.GetBytes(Command)).
                               Concat(BitConverter.GetBytes((Int32)(Parameters?.Length ?? 0))).
                               Concat(Parameters?.SelectMany(i => ObjectToArray(i)).ToArray() ?? new byte[] { }).
                               Concat(ObjectToArray(Result)).ToArray().Encrypt();

            return BitConverter.GetBytes((Int32)VibecodeHelpdekProtocolEnum.Command).Concat(encrypt_data).ToArray();
        }

        static object ParseToObject(byte[] data, ref int offset)
        {
            switch (data[offset++])
            {
                case 0:
                    return null;
                case 1:
                    return data[offset++];
                case 2:
                    int length = BitConverter.ToInt32(data, offset);
                    offset += sizeof(Int32);
                    var _bytes = new ArraySegment<byte>(data, offset, length).ToArray();
                    offset += length;
                    return _bytes;
                case 3:
                    var _int16 = BitConverter.ToInt16(data, offset);
                    offset += sizeof(Int16);
                    return _int16;
                case 4:
                    var _uint16 = BitConverter.ToUInt16(data, offset);
                    offset += sizeof(UInt16);
                    return _uint16;
                case 5:
                    var _int32 = BitConverter.ToInt32(data, offset);
                    offset += sizeof(Int32);
                    return _int32;
                case 6:
                    var _uint32 = BitConverter.ToUInt32(data, offset);
                    offset += sizeof(UInt32);
                    return _uint32;
                case 7:
                    var _int64 = BitConverter.ToInt64(data, offset);
                    offset += sizeof(Int64);
                    return _int64;
                case 8:
                    var _uint64 = BitConverter.ToUInt64(data, offset);
                    offset += sizeof(UInt64);
                    return _uint64;
                case 9:
                    var _float = BitConverter.ToSingle(data, offset);
                    offset += sizeof(float);
                    return _float;
                case 10:
                    var _double = BitConverter.ToDouble(data, offset);
                    offset += sizeof(float);
                    return _double;
                case 11:
                    var _bool = data[offset] == 1 ? true : false;
                    offset++;
                    return _bool;
                case 12:
                    length = BitConverter.ToInt32(data, offset);
                    offset += sizeof(Int32);
                    var _string = Encoding.UTF8.GetString(data, offset, length);
                    offset += length;
                    return _string;
                case 13:
                    var _dateTime = new DateTime(BitConverter.ToInt64(data, offset));
                    offset += sizeof(Int64);
                    return _dateTime;
                case 14:
                    var _guid = new Guid(new ArraySegment<byte>(data, offset, sizeof_Guid).ToArray());
                    offset += sizeof_Guid;
                    return _guid;
            }

            return null;
        }

        public static vhpCommand Parse(byte[] data)
        {
            try
            {
                var decript_data = data.Decrypt(sizeof(Int32));

                UInt32 p2p = BitConverter.ToUInt32(decript_data, 0);
                Int32 command = BitConverter.ToInt32(decript_data, sizeof(UInt32));

                List<object> parameters = null;
                int parameters_length = BitConverter.ToInt32(decript_data, sizeof(UInt32) + sizeof(Int32));
                int offset = sizeof(UInt32) + sizeof(Int32) + sizeof(Int32);

                if (parameters_length != 0)
                {
                    parameters = new List<object>();

                    for (int i = 0; i < parameters_length; i++)
                        parameters.Add(ParseToObject(decript_data, ref offset));
                }
                object result = ParseToObject(decript_data, ref offset);

                return new vhpCommand(p2p, (vhpCommandEnum)command, parameters?.ToArray(), result);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
    public class vhpStream : VibecodeHelpdeskProtocol
    {
        static byte id = 0x00;
        public byte Id { get; }
        public UInt32 Length { get; set; }
        public UInt16 PacketLength { get; set; }
        public string FileName { get; }
        public byte[] Buffer { get; private set; }
        WebSocketClient _webSocket;
        UInt32 _p2p;
        Stream _stream;
        public vhpStream(WebSocketClient webSocket, UInt32 p2p, UInt16 packetLength, Stream stream, string fileName = null)
        {
            id++;
            if (id == 0xFF)
                id = 0x00;
            Id = id;
            _webSocket = webSocket;
            _p2p = p2p;
            Length = (UInt32)(stream?.Length ?? 0);
            PacketLength = packetLength;
            FileName = fileName;
            _stream = stream;
        }
        public vhpStream(byte id, UInt32 length, UInt16 packetLength, byte[] buffer, string fileName)
        {
            Id = id;
            Length = length;
            PacketLength = packetLength;
            Buffer = buffer;
            FileName = fileName;
        }
        public override byte[] ToArray()
        {
            var encrypt_data = new byte[] { Id }.
                               Concat(BitConverter.GetBytes(Length)).
                               Concat(BitConverter.GetBytes(PacketLength)).
                               Concat(BitConverter.GetBytes((Int32)Encoding.UTF8.GetByteCount(FileName ?? string.Empty))).
                               Concat(Encoding.UTF8.GetBytes(FileName ?? string.Empty)).
                               Concat(Buffer != null ? Buffer : new byte[] { }).ToArray();
            encrypt_data = encrypt_data.Encrypt();

            return BitConverter.GetBytes((Int32)VibecodeHelpdekProtocolEnum.Stream).Concat(encrypt_data).ToArray();
        }

        /*public void Send()
        {
            if (_webSocket != null && _webSocket.IsAlive)
            {
                _stream.Position = 0;

                string _path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                var _tempName = Path.Combine(_path, "Temp.gzip");

                if (File.Exists(_tempName))
                    File.Delete(_tempName);

                using (FileStream _writeStream = new FileStream(_tempName, FileMode.CreateNew, FileAccess.ReadWrite))
                using (GZipStream _gzipStream = new GZipStream(_writeStream, CompressionLevel.Fastest))
                {
                    _stream.CopyTo(_gzipStream);
                };

                using (FileStream _readStream = new FileStream(_tempName, FileMode.Open, FileAccess.Read))
                {
                    var allInOne = _readStream.Length <= PacketLength;
                    if (allInOne)
                    {
                        Buffer = new byte[_readStream.Length];

                        _readStream.Position = 0;
                        _readStream.Read(Buffer, 0, Buffer.Length);
                    }
                    
                    Length = (UInt32)_readStream.Length;

                    byte[] message = ToArray();
                    _webSocket.Send(new vhpRedirect(_webSocket.Id, _p2p, message).ToArray());

                    if (Buffer == null)
                    {
                        _readStream.Position = 0;

                        Buffer = new byte[PacketLength];
                        int byte_read;

                        do
                        {
                            byte_read = _readStream.Read(Buffer, 0, Buffer.Length);

                            if (byte_read == Buffer.Length)
                                message = BitConverter.GetBytes((Int32)VibecodeHelpdekProtocolEnum.StreamPacket).
                                          Concat(new byte[] { Id }).
                                          Concat(Buffer).ToArray();
                            else
                                message = BitConverter.GetBytes((Int32)VibecodeHelpdekProtocolEnum.StreamPacket).
                                          Concat(new byte[] { Id }).
                                          Concat(new ArraySegment<byte>(Buffer, 0, byte_read).ToArray()).ToArray();

                            _webSocket.Send(new vhpRedirect(_webSocket.Id, _p2p, message).ToArray());
                        } while (byte_read == Buffer.Length);

                        Buffer = null;
                    }
                }

                File.Delete(_tempName);
                _webSocket = null;
            }
        }*/

        public static vhpStream Parse(byte[] data)
        {
            try
            {
                var decript_data = data.Decrypt(sizeof(Int32));

                byte id = decript_data[0];
                UInt32 length = BitConverter.ToUInt32(decript_data, sizeof_byte);
                UInt16 packetLength = BitConverter.ToUInt16(decript_data, sizeof_byte + sizeof(UInt32));
                int fileName_length = BitConverter.ToInt32(decript_data, sizeof_byte + sizeof(UInt32) + sizeof(UInt16));
                string fileName = null;
                if (fileName_length != 0)
                    fileName = Encoding.UTF8.GetString(decript_data, sizeof_byte + sizeof(UInt32) + sizeof(UInt16) + sizeof(Int32), fileName_length);
                byte[] buffer = null;
                if (decript_data.Length > sizeof_byte + sizeof(UInt32) + sizeof(UInt16) + sizeof(Int32) + fileName_length)
                    buffer = new ArraySegment<byte>(decript_data, sizeof_byte + sizeof(UInt32) + sizeof(UInt16) + sizeof(Int32) + fileName_length, (int)length).ToArray();

                return new vhpStream(id, length, packetLength, buffer, fileName);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
    public class vhpStreamPacket : VibecodeHelpdeskProtocol
    {
        public byte Id { get; }
        public byte[] Buffer { get; }

        public vhpStreamPacket(byte id, byte[] buffer)
        {
            Id = id;
            Buffer = buffer;
        }
        public override byte[] ToArray()
        {
            var data = new byte[] { Id }.
                       Concat(Buffer).ToArray();

            return BitConverter.GetBytes((Int32)VibecodeHelpdekProtocolEnum.StreamPacket).Concat(data).ToArray();
        }

        public static vhpStreamPacket Parse(byte[] data)
        {
            try
            {
                byte id = data[sizeof(Int32)];
                byte[] buffer = new ArraySegment<byte>(data, sizeof(Int32) + sizeof_byte, data.Length - (sizeof(Int32) + sizeof_byte)).ToArray();

                return new vhpStreamPacket(id, buffer);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
    public class vhpStreamPacketUdp : VibecodeHelpdeskProtocol
    {
        public byte Id { get; }
        public UInt32 Length { get; }
        public UInt16 PacketNumber { get; }
        public byte[] Buffer { get; }

        public vhpStreamPacketUdp(byte id, UInt32 length, UInt16 packetNumber, byte[] buffer)
        {
            Id = id;
            Length = length;
            PacketNumber = packetNumber;
            Buffer = buffer;
        }
        public override byte[] ToArray()
        {
            var data = new byte[] { Id }.
                       Concat(BitConverter.GetBytes(Length)).
                       Concat(BitConverter.GetBytes(PacketNumber)).
                       Concat(Buffer).ToArray();

            return BitConverter.GetBytes((Int32)VibecodeHelpdekProtocolEnum.StreamPacketUdp).Concat(data).ToArray();
        }

        public static vhpStreamPacketUdp Parse(byte[] data)
        {
            try
            {
                byte id = data[sizeof(Int32)];
                UInt32 length = BitConverter.ToUInt32(data, sizeof(Int32) + sizeof_byte);
                UInt16 packetNumber = BitConverter.ToUInt16(data, sizeof(Int32) + sizeof_byte + sizeof(UInt32));
                byte[] buffer = new ArraySegment<byte>(data, sizeof(Int32) + sizeof_byte + sizeof(UInt32) + sizeof(UInt16), data.Length - (sizeof(Int32) + sizeof_byte + sizeof(UInt32) + sizeof(UInt16))).ToArray();

                return new vhpStreamPacketUdp(id, length, packetNumber, buffer);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
    public class vhpDisconnect : VibecodeHelpdeskProtocol
    {
        public UInt32 P2PId { get; }
        public vhpDisconnect(UInt32 p2p = 0)
        {
            P2PId = p2p;
        }

        public override byte[] ToArray()
        {
            return BitConverter.GetBytes((Int32)VibecodeHelpdekProtocolEnum.Disconnect).Concat(BitConverter.GetBytes(P2PId)).ToArray();
        }

        public static vhpDisconnect Parse(byte[] data)
        {
            try
            {
                UInt32 p2p = BitConverter.ToUInt32(data, sizeof(Int32));

                return new vhpDisconnect(p2p);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
    public class vhpError : VibecodeHelpdeskProtocol
    {
        public Int32 Code { get; }
        public string Message { get; }

        public vhpError(int code, string message)
        {
            Code = code;
            Message = message;
        }

        public override byte[] ToArray()
        {
            var encrypt_data = BitConverter.GetBytes(Code).
                               Concat(BitConverter.GetBytes((Int32)Encoding.UTF8.GetByteCount(Message ?? string.Empty))).
                               Concat(Encoding.UTF8.GetBytes(Message ?? string.Empty)).ToArray().Encrypt();

            return BitConverter.GetBytes((Int32)VibecodeHelpdekProtocolEnum.Error).Concat(encrypt_data).ToArray();
        }

        public static vhpError Parse(byte[] data)
        {
            try
            {
                var decript_data = data.Decrypt(sizeof(Int32));

                Int32 code = BitConverter.ToInt32(decript_data, 0);
                int message_length = BitConverter.ToInt32(decript_data, sizeof(Int32));
                string message = null;
                if (message_length != 0)
                    message = Encoding.UTF8.GetString(decript_data, sizeof(Int32) + sizeof(Int32), message_length);

                return new vhpError(code, message);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
    public class vhpOutputLine : VibecodeHelpdeskProtocol
    {
        public string Line { get; }

        public vhpOutputLine(string line)
        {
            Line = line;
        }

        public override byte[] ToArray()
        {
            var encrypt_data = BitConverter.GetBytes((Int32)Encoding.UTF8.GetByteCount(Line ?? string.Empty)).
                               Concat(Encoding.UTF8.GetBytes(Line ?? string.Empty)).ToArray().Encrypt();

            return BitConverter.GetBytes((Int32)VibecodeHelpdekProtocolEnum.OutputLine).Concat(encrypt_data).ToArray();
        }

        public static vhpOutputLine Parse(byte[] data)
        {
            try
            {
                var decript_data = data.Decrypt(sizeof(Int32));

                int line_length = BitConverter.ToInt32(decript_data, 0);
                string line = null;
                if (line_length != 0)
                    line = Encoding.UTF8.GetString(decript_data, sizeof(Int32), line_length);

                return new vhpOutputLine(line);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
    public class vhpChat : VibecodeHelpdeskProtocol
    {
        public enum ChatStstusEnum { Sent = 0x00, Received = 0x01, Read = 0x02 }
        public Guid Id { get; }
        public UInt32 SenderId { get; }
        public UInt32 SenderP2P { get; }
        public string Message { get; }
        public DateTime Date { get; }
        public string Sender { get; set; }
        public string Time => Date.ToString("HH:mm");
        public ChatStstusEnum Status { get; set; }
        public string Status_ToString => Status == ChatStstusEnum.Sent ? Resources.Alias.done : Resources.Alias.done_all;
        public System.Windows.Media.Brush Status_Color => Status == ChatStstusEnum.Read ? new SolidColorBrush(System.Windows.Media.Color.FromArgb(0xFF, 0x1A, 0x8E, 0xFF)) : System.Windows.Media.Brushes.Silver;
        public string GroupBy => !string.IsNullOrEmpty(Sender) && Status != ChatStstusEnum.Read ? "Messaggi non letti" : Date.ToString("dd MMMM yyyy");
        public vhpChat(UInt32 senderId, UInt32 senderP2P, string message, DateTime dateTime, ChatStstusEnum status, Guid? id = null)
        {
            Id = id ?? Guid.NewGuid();
            SenderId = senderId;
            SenderP2P = senderP2P;
            Message = message;
            Date = dateTime;
            Status = status;
        }

        public override byte[] ToArray()
        {
            var encrypt_data = Id.ToByteArray().
                               Concat(BitConverter.GetBytes(SenderId)).
                               Concat(BitConverter.GetBytes(SenderP2P)).
                               Concat(BitConverter.GetBytes((Int32)Encoding.UTF8.GetByteCount(Message ?? string.Empty))).
                               Concat(Encoding.UTF8.GetBytes(Message ?? string.Empty)).
                               Concat(BitConverter.GetBytes((Int64)Date.Ticks)).
                               Concat(new byte[] { (byte)Status }).ToArray().Encrypt();

            return BitConverter.GetBytes((Int32)VibecodeHelpdekProtocolEnum.Chat).Concat(encrypt_data).ToArray();
        }

        public static vhpChat Parse(byte[] data)
        {
            try
            {
                var decript_data = data.Decrypt(sizeof(Int32));

                Guid guid = new Guid(new ArraySegment<byte>(decript_data, 0, sizeof_Guid).ToArray());
                UInt32 senderId = BitConverter.ToUInt32(decript_data, sizeof_Guid);
                UInt32 senderP2P = BitConverter.ToUInt32(decript_data, sizeof_Guid + sizeof(UInt32));
                int message_length = BitConverter.ToInt32(decript_data, sizeof_Guid + sizeof(UInt32) + sizeof(UInt32));
                string message = null;
                if (message_length != 0)
                    message = Encoding.UTF8.GetString(decript_data, sizeof_Guid + sizeof(UInt32) + sizeof(UInt32) + sizeof(Int32), message_length);
                DateTime dateTime = new DateTime(BitConverter.ToInt64(decript_data, sizeof_Guid + sizeof(UInt32) + sizeof(UInt32) + sizeof(Int32) + message_length));
                ChatStstusEnum status = (ChatStstusEnum)decript_data[sizeof_Guid + sizeof(UInt32) + sizeof(UInt32) + sizeof(Int32) + message_length + sizeof(Int64)];

                return new vhpChat(senderId, senderP2P, message, dateTime, status, guid);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
