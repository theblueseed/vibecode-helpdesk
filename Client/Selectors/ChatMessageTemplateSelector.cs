﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Vibecode_Helpdesk
{
    public class ChatMessageTemplateSelector : DataTemplateSelector
    {
        public DataTemplate InMessageTemplate { get; set; }
        public DataTemplate OutMessageTemplate { get; set; }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            if (item is vhpChat message)
            {
                if (string.IsNullOrEmpty(message.Sender))
                    return OutMessageTemplate;

                return InMessageTemplate;
            }
            return null;
        }
    }
}
