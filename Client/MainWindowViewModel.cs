﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace Vibecode_Helpdesk
{
    public class SessionViewModel : INotifyPropertyChanged
    {
        Color statusColor;
        public Color StatusColor
        {
            get => statusColor;
            set => SetField(ref statusColor, value);
        }

        string status;
        public string Status
        {
            get => status;
            set => SetField(ref status, value);
        }

        string id;
        public string ID
        {
            get => id;
            set => SetField(ref id, value);
        }

        string peerId;
        public string PeerID
        {
            get => peerId;
            set
            {
                SetField(ref peerId, value);
                ConnectCommand.RaiseCanExecuteChanged();
            }
        }

        VibecodeHelpdeskP2PInfo connectedInfo;
        public VibecodeHelpdeskP2PInfo ConnectedInfo
        {
            get => connectedInfo;
            set => SetField(ref connectedInfo, value);
        }

        double downloadProgress;
        public double DownloadProgress
        {
            get => downloadProgress;
            set => SetField(ref downloadProgress, value);
        }

        double downloadProgressMax;
        public double DownloadProgressMax
        {
            get => downloadProgressMax;
            set => SetField(ref downloadProgressMax, value);
        }

        Visibility downloadProgressIsVisible = Visibility.Collapsed;
        public Visibility DownloadProgressIsVisible
        {
            get => downloadProgressIsVisible;
            set => SetField(ref downloadProgressIsVisible, value);
        }

        public RelayCommand ConnectCommand { get; private set; }
        public RelayCommand PlayCommand { get; private set; }
        public RelayCommand PauseCommand { get; private set; }
        public RelayCommand DisconnectCommand { get; private set; }
        public RelayCommand CaptureCommand { get; private set; }
        public RelayCommand DownloadCommand { get; private set; }
        public RelayCommand ExplorerCommand { get; private set; }
        public RelayCommand SaveOutputLinesCommand { get; private set; }
        public RelayCommand ClearOutputLinesCommand { get; private set; }
        public RelayCommand EnableAutoScrollCommand { get; private set; }
        public RelayCommand SendMessageCommand { get; private set; }

        public ObservableCollection<string> OutputLines { get; } = new ObservableCollection<string>();
        public bool EnableAutoScroll { get; set; } = true;

        bool _connected;
        bool _peered;
        bool _downloaded;
        public bool _play;
        public bool _pause;
        WebSocketClient ws;
        readonly MainWindowViewModel ViewModel;

        string message;
        public string Message
        {
            get => message;
            set
            {
                SetField(ref message, value);
                SendMessageCommand?.RaiseCanExecuteChanged();
            }
        }

        Timer timer;
        bool busy;

        public SessionViewModel(MainWindowViewModel main)
        {
            ViewModel = main;

            Status = "Connecting...";
            StatusColor = Color.Orange;

            timer = new Timer(1000);
            timer.AutoReset = true;
            timer.Elapsed += Timer_Elapsed;

            SendMessageCommand = new RelayCommand(() =>
            {
                if (busy)
                    return;
                busy = true;

                SendMessage(message, ConnectedInfo.P2PId);
                Message = null;

                busy = false;
            },
            () => !string.IsNullOrEmpty(message));

            SaveOutputLinesCommand = new RelayCommand(() =>
            {
                var FileName = Path.Combine(ws.BasePath, $"OUT_{DateTime.Now.ToString("yyyyMMddHHmmss")}.txt");
                File.WriteAllLines(FileName, OutputLines.ToList());
            },
            () => _peered && OutputLines.Count > 0);

            ClearOutputLinesCommand = new RelayCommand(() =>
            {
                OutputLines.Clear();
                SaveOutputLinesCommand.RaiseCanExecuteChanged();
                ClearOutputLinesCommand.RaiseCanExecuteChanged();
            },
            () => _peered && OutputLines.Count > 0);

            EnableAutoScrollCommand = new RelayCommand(() =>
            {
                EnableAutoScroll = !EnableAutoScroll;
                ViewModel.Window.EnableAutoScroll.IsChecked = EnableAutoScroll;
            },
            () => _peered);

            DownloadCommand = new RelayCommand(() =>
            {
                if (_downloaded)
                    return;
                _downloaded = true;

                DownloadCommand.RaiseCanExecuteChanged();

                var id = Convert.ToUInt32(ID);
                var p2p = ws.GetP2PId(id);

                var cmd = new vhpCommand(p2p, vhpCommand.vhpCommandEnum.Download, new object[] { });
                var message = new vhpRedirect(id, p2p, cmd.ToArray());
                ws.Send(message.ToArray());
            },
            () => _peered && !_downloaded);

            ExplorerCommand = new RelayCommand(() =>
            {
                Process.Start(ws.BasePath);
            },
            () => _peered);


            CaptureCommand = new RelayCommand(() =>
            {
                PauseCommand.Execute(null);

                var FileName = Path.Combine(ws.BasePath, $"IMG_{DateTime.Now.ToString("yyyyMMddHHmmss")}.png");
                /*var op = new System.Windows.Forms.SaveFileDialog
                {
                    Title = "Salva con nome",
                    Filter = "Immagine PNG|*.png|" +
                             "Immagine JPEG|*.jpeg|" +
                             "Immagine BMP|*.bmp"
                };

                if (op.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    if (!string.IsNullOrEmpty(op.FileName))
                    {*/
                switch (Path.GetExtension(FileName))
                {
                    case ".png":
                        ViewModel.SaveToPng(Image, FileName);
                        break;
                    case ".jpeg":
                        ViewModel.SaveToJpeg(Image, FileName);
                        break;
                    case ".bmp":
                        ViewModel.SaveToBmp(Image, FileName);
                        break;
                }
                /*}
            }*/

                PauseCommand.Execute(null);
            },
            () => Image != null);

            ConnectCommand = new RelayCommand(() =>
            {
                OutputLines.Clear();
                Messages.Clear();
                EnableAutoScroll = true;
                ViewModel.Window.EnableAutoScroll.IsChecked = true;

                try
                {
                    var id = Convert.ToUInt32(ID);
                    var p2p = Convert.ToUInt32(PeerID);

                    _connected = true;
                    ConnectCommand.RaiseCanExecuteChanged();

                    ViewModel.IsBusy = true;

                    var peer2peer = new vhpPeer2Peer(id, p2p).ToArray();
                    ws.Send(peer2peer);
                }
                catch
                { }
            },
            () => !string.IsNullOrEmpty(ID) && !string.IsNullOrEmpty(PeerID) && !_connected);

            DisconnectCommand = new RelayCommand(() =>
            {
                try
                {
                    ResetWS();
                }
                catch
                { }
            },
            () => _peered);

            PlayCommand = new RelayCommand(() =>
            {
                _play = !_play;
                ViewModel.Window.Play.IsChecked = _play;

                if (_play)
                {
                    ViewModel.Window.Play.ToolTip = "Stop";
                    ViewModel.Window.Play.Content = Resources.Alias.stop;

                    if (_pause)
                    {
                        _pause = false;
                        ViewModel.Window.Pause.IsChecked = false;
                        ViewModel.Window.Pause.ToolTip = "Pause";
                        ViewModel.Window.Pause.Content = Resources.Alias.pause;
                    }
                }
                else
                {
                    ViewModel.Window.Play.ToolTip = "Play";
                    ViewModel.Window.Play.Content = Resources.Alias.play_arrow;

                    if (_pause)
                    {
                        ViewModel.Window.Pause.ToolTip = "Stop";
                        ViewModel.Window.Pause.Content = Resources.Alias.stop;
                    }
                }

                PauseCommand.RaiseCanExecuteChanged();

                var id = Convert.ToUInt32(ID);
                var p2p = ws.GetP2PId(id);

                var cmd = new vhpCommand(p2p, vhpCommand.vhpCommandEnum.ScreenCapture, new object[] { _play });
                var message = new vhpRedirect(id, p2p, cmd.ToArray());
                ws.Send(message.ToArray());

                UdpSocketClient.Packets.Clear();
                UdpSocketClient.Frame = -1;
                FPS = string.Empty;

                if (_play)
                    timer.Start();
                else
                    timer.Stop();

                if (!_play && !_pause)
                    Image = null;
            },
            () => _peered);

            PauseCommand = new RelayCommand(() =>
            {
                _pause = !_pause;
                if (_pause)
                {
                    ViewModel.Window.Pause.ToolTip = "Play";
                    ViewModel.Window.Pause.Content = Resources.Alias.play_arrow;
                }
                else
                {
                    ViewModel.Window.Pause.ToolTip = "Pause";
                    ViewModel.Window.Pause.Content = Resources.Alias.pause;
                }
                PauseCommand.RaiseCanExecuteChanged();

                if (!_play)
                {
                    Image = null;
                }
            },
            () => _peered && (_play || _pause));

            ConnectCommand.RaiseCanExecuteChanged();
            PauseCommand.RaiseCanExecuteChanged();
            DownloadCommand.RaiseCanExecuteChanged();
            ExplorerCommand.RaiseCanExecuteChanged();

            Start();
        }

        public void Start()
        {
            var ip = "52.136.194.160"; //"192.168.8.102"; //
            ws = new WebSocketClient(ip, 19095);

            ws.OnConnected += OnConnected;
            ws.OnDisconnected += OnDisconnected;
            ws.OnSubscribe += OnSubscribe;
            ws.OnPeer2Peer += OnPeer2Peer;
            ws.OnDownload += OnDownload;
            ws.OnError += OnError;
            ws.OnBlinking += OnBlinking;
            ws.OnOuputLine += OnOuputLine;
            ws.OnDownloadProgress += OnDownloadProgress;
            ws.OnCommand += OnCommand;
            ws.OnChat += OnChat;

            ws.BasePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "Vibecode Helpdesk");
            if (!Directory.Exists(ws.BasePath))
                Directory.CreateDirectory(ws.BasePath);

            Task.Run(() =>
            {
                App.manualReset.WaitOne();
                ws.Start();
            });
        }

        public ObservableCollection<vhpChat> Messages { get; private set; } = new ObservableCollection<vhpChat>();
        public bool HasMessageUnread => ws.IsAlive && Messages.Any(i => !string.IsNullOrEmpty(i.Sender) && i.Status != vhpChat.ChatStstusEnum.Read);
        public int MessageUnread => Messages.Count(i => !string.IsNullOrEmpty(i.Sender) && i.Status != vhpChat.ChatStstusEnum.Read);

        private void OnChat(object sender, vhpChat e)
        {
            Application.Current.Dispatcher.Invoke((Action)delegate
            {
                if (e.Sender == null)
                {
                    if (Messages.ToList().Where(i => i.Id == e.Id).FirstOrDefault() is vhpChat message)
                        message.Status = e.Status;

                    ViewModel.Window.listView.Items.Refresh();
                    //OnPropertyChanged(nameof(Messages));
                }
                else
                {
                    if (e.Status == vhpChat.ChatStstusEnum.Sent)
                    {
                        e.Status = vhpChat.ChatStstusEnum.Read;
                        SendNotify(e);
                    }

                    Messages.Add(e);
                }
                
                OnPropertyChanged(nameof(MessageUnread));
                OnPropertyChanged(nameof(HasMessageUnread));

                Console.WriteLine(e.Message);
            });
        }

        public void SendMessage(string message, UInt32 p2p)
        {
            var chat = new vhpChat(Convert.ToUInt32(ID), p2p, message, DateTime.Now, vhpChat.ChatStstusEnum.Sent);
            var chat_array = chat.ToArray();

            Messages.Add(chat);

            var redirect = new vhpRedirect(Convert.ToUInt32(ID), chat.SenderP2P, chat.ToArray());
            ws.Send(redirect.ToArray());
        }

        public void SendNotify(vhpChat message)
        {
            var redirect = new vhpRedirect(Convert.ToUInt32(ID), message.SenderP2P, message.ToArray());
            ws.Send(redirect.ToArray());
        }

        private void OnCommand(object sender, vhpCommand e)
        {
            switch ((vhpCommand.vhpCommandEnum)e.Command)
            {
                case vhpCommand.vhpCommandEnum.GetInfo:
                    var json = JObject.Parse(e.Result.ToString());

                    Application.Current.Dispatcher.Invoke((Action)delegate
                    {
                        ViewModel.Window.PropertyGrid.SelectedObject = json;
                    });

                    break;

                case vhpCommand.vhpCommandEnum.ScreenCapture:
                case vhpCommand.vhpCommandEnum.Download:
                    Console.WriteLine($"Command Result: {e.Result}");
                    break;
            }
        }

        int old_frame = -1;
        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            var frame = UdpSocketClient.Frame;

            Application.Current.Dispatcher.Invoke((Action)delegate
            {
                if (old_frame == -1 || old_frame > frame)
                    FPS = string.Empty;
                else
                    FPS = $"{frame - old_frame} fps";
            });

            old_frame = UdpSocketClient.Frame;
            return;
        }
        string fps;
        public string FPS
        {
            get => fps;
            set => SetField(ref fps, value);
        }

        private void OnDownloadProgress(object sender, int e)
        {
            if (e < 0)
            {
                Application.Current.Dispatcher.Invoke((Action)delegate
                {
                    DownloadProgress = 0;
                    DownloadProgressMax = Convert.ToDouble(-e);

                    DownloadProgressIsVisible = Visibility.Visible;
                });
            }
            else
            {
                Application.Current.Dispatcher.Invoke((Action)delegate
                {
                    DownloadProgress = Convert.ToDouble(e) / downloadProgressMax * 100.0;
                    if (e == 0)
                    {
                        DownloadProgressIsVisible = Visibility.Collapsed;

                        _downloaded = false;
                        DownloadCommand.RaiseCanExecuteChanged();
                    }
                });
            }
        }

        private void OnOuputLine(object sender, string e)
        {
            Application.Current.Dispatcher.Invoke((Action)delegate
            {
                OutputLines.Add(e);
                SaveOutputLinesCommand.RaiseCanExecuteChanged();
                ClearOutputLinesCommand.RaiseCanExecuteChanged();
            });
        }

        private async void OnBlinking(object sender, bool e)
        {
            Application.Current.Dispatcher.Invoke((Action)delegate
            {
                if (e)
                    StatusColor = Color.LightGreen;
                else
                    StatusColor = Color.GreenYellow;
            });
            await Task.Delay(300);
            Application.Current.Dispatcher.Invoke((Action)delegate
            {
                StatusColor = Color.Green;
            });
        }

        private void OnError(object sender, vhpError e)
        {
            Application.Current.Dispatcher.Invoke((Action)delegate
            {
                if (!_peered && _connected)
                    ResetConnect();
                
                ViewModel.IsBusy = false;

                MessageBox.Show(e.Message);
            });
        }

        private void OnDownload(object sender, StreamPacket e)
        {
            if (!string.IsNullOrEmpty(e.FileName) || (_play && !_pause))
                SetFrame(e);
        }

        private void OnDisconnected(object sender, EventArgs e)
        {
            Application.Current.Dispatcher.Invoke((Action)delegate
            {
                Status = "Connecting..."; 
                StatusColor = Color.Orange;
            });
        }

        private void OnConnected(object sender, EventArgs e)
        {
            Application.Current.Dispatcher.Invoke((Action)delegate
            {
                Status = "Connected";
                StatusColor = Color.Green;
            });
        }

        void ResetWS()
        {
            ws.Disconnect();
            ws.Dispose();
            ws = null;

            _peered = false;
            ResetConnect();

            Start();
        }

        private void OnPeer2Peer(object sender, vhpSubscribeP2P e)
        {
            Application.Current.Dispatcher.Invoke((Action)delegate
            {
                DownloadProgressIsVisible = Visibility.Collapsed;

                if (e == null)
                {
                    ResetWS();
                    _peered = false;
                    _connected = false;
                    _play = false;
                    _downloaded = false;
                    PlayCommand.RaiseCanExecuteChanged();
                    DisconnectCommand.RaiseCanExecuteChanged();
                    ConnectCommand.RaiseCanExecuteChanged();
                    DownloadCommand.RaiseCanExecuteChanged();
                    ExplorerCommand.RaiseCanExecuteChanged();

                    SaveOutputLinesCommand.RaiseCanExecuteChanged();
                    ClearOutputLinesCommand.RaiseCanExecuteChanged();
                    EnableAutoScrollCommand.RaiseCanExecuteChanged();

                    ViewModel.Connected = false;

                    Status = "Connected";
                    StatusColor = Color.Green;

                    ViewModel.IsBusy = false;
                }
                else
                {
                    _peered = true;
                    PlayCommand.RaiseCanExecuteChanged();
                    DisconnectCommand.RaiseCanExecuteChanged();
                    DownloadCommand.RaiseCanExecuteChanged();
                    ExplorerCommand.RaiseCanExecuteChanged();

                    SaveOutputLinesCommand.RaiseCanExecuteChanged();
                    ClearOutputLinesCommand.RaiseCanExecuteChanged();
                    EnableAutoScrollCommand.RaiseCanExecuteChanged();

                    ViewModel.Connected = true;
                    ViewModel.IsBusy = false;

                    ConnectedInfo = ws.GetConnectedInfo(e.Id);

                    if (e.IsLocalP2P)
                        Status = $"Connected to {e.RemoteId} [{e.LocalP2PEndPoint.Address}:{e.LocalP2PEndPoint.Port}]";
                    else
                    {
                        if (e.PublicP2PEndPoint == null)
                            Status = $"Connected to {e.RemoteId}";
                        else
                            Status = $"Connected to {e.RemoteId} [{e.PublicP2PEndPoint.Address}:{e.PublicP2PEndPoint.Port}]";
                    }

                    var getInfo = new vhpCommand(e.P2PId, vhpCommand.vhpCommandEnum.GetInfo, new object[] { });
                    var message = new vhpRedirect(e.Id, e.P2PId, getInfo.ToArray());
                    ws.Send(message.ToArray());
                }
            });
        }

        private void OnSubscribe(object sender, vhpSubscribe e)
        {
            Application.Current.Dispatcher.Invoke((Action)delegate
            {
                ID = e.Id.ToString();
                ConnectCommand.RaiseCanExecuteChanged();
            });
        }

        public void ResetConnect()
        {
            _connected = false;
            ConnectCommand.RaiseCanExecuteChanged();
        }

        public void SetFrame(StreamPacket packet)
        {
            if (string.IsNullOrEmpty(packet.FileName))
            {
                using (var stream = new MemoryStream(packet.Buffer))
                {
                    var bitmap = new BitmapImage();
                    bitmap.BeginInit();
                    bitmap.StreamSource = stream;
                    bitmap.CacheOption = BitmapCacheOption.OnLoad;
                    try
                    {
                        bitmap.EndInit();
                        bitmap.Freeze();

                        Application.Current.Dispatcher.Invoke((Action)delegate
                        {
                            Image = bitmap;
                        });
                    }
                    catch { }
                }
            }
            else
            {
                var fileName = Path.Combine(ws.BasePath, packet.FileName);

                int i = 0;
                while (File.Exists(fileName))
                    fileName = Path.Combine(ws.BasePath, $"{Path.GetFileNameWithoutExtension(packet.FileName)} ({++i}){Path.GetExtension(packet.FileName)}");

                using (var fs = new FileStream(fileName, FileMode.CreateNew))
                {
                    fs.Write(packet.Buffer, 0, (int)packet.Buffer.Length);
                    fs.Flush();
                    fs.Close();
                }
            }
        }

        private BitmapImage image;
        public BitmapImage Image
        {
            get => image;
            set
            {
                SetField(ref image, value);
                CaptureCommand.RaiseCanExecuteChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        protected bool SetField<T>(ref T field, T value, [CallerMemberName] string propertyName = "")
        {
            if (EqualityComparer<T>.Default.Equals(field, value)) return false;
            field = value;
            OnPropertyChanged(propertyName);
            return true;
        }
    }

    public class MainWindowViewModel : INotifyPropertyChanged
    {
        bool isBusy = false;
        public bool IsBusy
        {
            get => isBusy;
            set => SetField(ref isBusy, value);
        }

        SessionViewModel current;
        public SessionViewModel Current 
        {
            get => current;
            set => SetField(ref current, value);
        }

        public void SaveToBmp(FrameworkElement visual, string fileName)
        {
            var encoder = new BmpBitmapEncoder();
            SaveUsingEncoder(visual, fileName, encoder);
        }

        public void SaveToPng(FrameworkElement visual, string fileName)
        {
            var encoder = new PngBitmapEncoder();
            SaveUsingEncoder(visual, fileName, encoder);
        }

        public void SaveToJpeg(FrameworkElement visual, string fileName)
        {
            var encoder = new JpegBitmapEncoder();
            SaveUsingEncoder(visual, fileName, encoder);
        }

        void SaveUsingEncoder(FrameworkElement visual, string fileName, BitmapEncoder encoder)
        {
            RenderTargetBitmap bitmap = new RenderTargetBitmap((int)visual.ActualWidth, (int)visual.ActualHeight, 96, 96, System.Windows.Media.PixelFormats.Pbgra32);
            bitmap.Render(visual);
            BitmapFrame frame = BitmapFrame.Create(bitmap);
            encoder.Frames.Add(frame);

            using (var stream = File.Create(fileName))
            {
                encoder.Save(stream);
            }
        }

        public void SaveToBmp(BitmapImage image, string fileName)
        {
            var encoder = new BmpBitmapEncoder();
            SaveUsingEncoder(image, fileName, encoder);
        }

        public void SaveToPng(BitmapImage image, string fileName)
        {
            var encoder = new PngBitmapEncoder();
            SaveUsingEncoder(image, fileName, encoder);
        }

        public void SaveToJpeg(BitmapImage image, string fileName)
        {
            var encoder = new JpegBitmapEncoder();
            SaveUsingEncoder(image, fileName, encoder);
        }

        void SaveUsingEncoder(BitmapImage image, string fileName, BitmapEncoder encoder)
        {
            encoder.Frames.Add(BitmapFrame.Create(image));

            using (var fileStream = new System.IO.FileStream(fileName, System.IO.FileMode.Create))
            {
                encoder.Save(fileStream);
            }
        }
        
        bool connected;
        public bool Connected
        {
            get => connected;
            set
            {
                if (value)
                {
                    Current._play = false;
                    Current._pause = false;

                    Window.Play.IsChecked = false;
                    Window.Play.Content = Vibecode_Helpdesk.Resources.Alias.play_arrow;
                    Window.Pause.IsChecked = false;
                    Window.Pause.Content = Vibecode_Helpdesk.Resources.Alias.pause;

                    Current.PlayCommand.RaiseCanExecuteChanged();
                    Current.PauseCommand.RaiseCanExecuteChanged();

                    Current.Image = null;
                    Window.OnConnected();
                }

                SetField(ref connected, value);
                OnPropertyChanged(nameof(NotConnected));
            }
        }
        public bool NotConnected => !connected;

        public readonly MainWindow Window;
        public MainWindowViewModel(MainWindow window)
        {
            Window = window;
            Current = new SessionViewModel(this);
            Connected = false;

            Avatar = App.GetUserTilePath(null);
        }

        string avatar;
        public string Avatar
        {
            get => avatar;
            set => SetField(ref avatar, value);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        protected bool SetField<T>(ref T field, T value, [CallerMemberName] string propertyName = "")
        {
            if (EqualityComparer<T>.Default.Equals(field, value)) return false;
            field = value;
            OnPropertyChanged(propertyName);
            return true;
        }
    }
}
