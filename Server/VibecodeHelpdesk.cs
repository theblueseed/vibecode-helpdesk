﻿using System;
using System.Net;
using System.Linq;
using WebSocketSharper;
using WebSocketSharper.Server;
using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace HolePunching
{
    public class Subscriber
    {
        public VibecodeHelpdesk VibecodeHelpdesk { get; private set; }
        public UInt32 Id { get; private set; }
        public IPEndPoint PublicEndPoint { get; set; }
        public IPEndPoint LocalEndPoint { get; set; }
        public string FullName { get; private set; }
        public byte[] Avatar { get; private set; }
        public bool Closing { get; set; } = false;
        public Subscriber(VibecodeHelpdesk vibecodeHelpdesk, UInt32 id, string fullName, byte[] avatar)
        {
            VibecodeHelpdesk = vibecodeHelpdesk;
            Id = id;
            FullName = fullName;
            Avatar = avatar;
        }
    }

    public class Peer2Peer
    {
        public UInt32 Id { get; private set; }
        public UInt32 SubscriberId1 { get; set; }
        public UInt32 SubscriberId2 { get; set; }

        public Subscriber Subscriber1
        {
            get
            {
                if (VibecodeHelpdesk.Subscriptions.TryGetValue(SubscriberId1, out Subscriber subscriber))
                    return subscriber;
                else
                    return null;
            }
        }
        public Subscriber Subscriber2
        {
            get
            {
                if (VibecodeHelpdesk.Subscriptions.TryGetValue(SubscriberId2, out Subscriber subscriber))
                    return subscriber;
                else
                    return null;
            }
        }

        public Peer2Peer(UInt32 subscriberId1, UInt32 subscriberId2)
        {
            SubscriberId1 = subscriberId1;
            SubscriberId2 = subscriberId2;

            Id = (UInt32)$"{subscriberId1} {subscriberId2}".GetHashCode();
        }
    }

    public class VibecodeHelpdesk : WebSocketBehavior
    {
        public static ConcurrentDictionary<uint, Subscriber> Subscriptions { get; private set; } = new ConcurrentDictionary<uint, Subscriber>();
        public static ConcurrentDictionary<uint, Peer2Peer> Peer2Peers { get; private set; } = new ConcurrentDictionary<uint, Peer2Peer>();
        public static IPEndPoint UDPEndPoint { get; set; }

        IPEndPoint userEndPoint;
        UInt32 id;
        bool closing = false;

        protected override void OnOpen()
        {
            userEndPoint = Context.UserEndPoint;
            Console.WriteLine($"OnOpen {userEndPoint.Address}, {userEndPoint.Port}");
        }

        protected override void OnMessage(MessageEventArgs e)
        {
            if (e.IsBinary)
            {
                byte[] result = EngageData(e.RawData);
                if (result != null)
                    Send(result);
            }
            else if (e.IsText)
            {
                Console.WriteLine($"OnMessage: Text={e.Data}");
            }
            else if (e.IsPing)
            {
                Console.WriteLine($"OnMessage: Ping");
            }
        }

        protected override void OnError(ErrorEventArgs e)
        {
            Console.WriteLine($"OnError: e.Message={e.Message}, e.Exception={e.Exception}");
        }

        protected override void OnClose(CloseEventArgs e)
        {
            Console.WriteLine($"OnClose: {id} [{userEndPoint.Address}:{userEndPoint.Port}]");

            if (Subscriptions.TryGetValue(id, out Subscriber removing))
                removing.Closing = true;

            Task.Run(async () =>
            {
                if (!closing)
                    await Task.Delay(30000);

                if (Subscriptions.TryGetValue(id, out removing))
                {
                    if (!removing.Closing)
                    {
                        Console.WriteLine($"Reconnected: {id} [{userEndPoint.Address}:{userEndPoint.Port}]");
                        return;
                    }
                }

                Console.WriteLine($"Close: {id} [{userEndPoint.Address}:{userEndPoint.Port}]");

                if (Subscriptions.TryRemove(id, out Subscriber removed))
                {
                    Console.WriteLine($"Unsubscribe: {id} [{userEndPoint.Address}:{userEndPoint.Port}]");
                    CloseP2P(removed);
                }
            });
        }

        private void CloseP2P(Subscriber removed)
        {
            removed.LocalEndPoint = null;
            removed.PublicEndPoint = null;

            foreach (var p2p in Peer2Peers.Values.ToList().Where(i => i.SubscriberId1 == id || i.SubscriberId2 == id))
            {
                if (p2p.SubscriberId1 == id)
                {
                    if (p2p.Subscriber2 is Subscriber subscritor)
                    {
                        subscritor.LocalEndPoint = null;
                        subscritor.PublicEndPoint = null;
                        subscritor.VibecodeHelpdesk.Send(new vhpDisconnect(p2p.Id).ToArray());
                    }
                    else
                    {
                        Console.WriteLine($"Disconnect Peer2Peer: {p2p.Id}");
                        Peer2Peers.TryRemove(p2p.Id, out _);
                    }
                }
                else if (p2p.SubscriberId2 == id)
                {
                    if (p2p.Subscriber1 is Subscriber subscritor)
                    {
                        subscritor.LocalEndPoint = null;
                        subscritor.PublicEndPoint = null;

                        subscritor.VibecodeHelpdesk.Send(new vhpDisconnect(p2p.Id).ToArray());
                    }
                    else
                    {
                        Console.WriteLine($"Disconnect Peer2Peer: {p2p.Id}");
                        Peer2Peers.TryRemove(p2p.Id, out _);
                    }
                }
            }
        }

        private byte[] EngageData(byte[] data)
        {
            switch ((VibecodeHelpdekProtocolEnum)BitConverter.ToInt32(data, 0))
            {
                case VibecodeHelpdekProtocolEnum.Acknowledgement:
                    Console.WriteLine($"Acknowledgement [{userEndPoint.Address}:{userEndPoint.Port}]");
                    return null;

                case VibecodeHelpdekProtocolEnum.Subscribe:
                    if (vhpSubscribe.Parse(data) is vhpSubscribe subscribe)
                    {
                        subscribe.Id = id = (UInt32)$"{userEndPoint.Address} {subscribe.LocalIP} {subscribe.Session}".GetHashCode();

                        Console.WriteLine($"Subscribe {id} [{userEndPoint.Address}:{userEndPoint.Port}]");

                        var subscription = new Subscriber(this, id, subscribe.FullName, subscribe.Avatar);
                        Subscriptions.AddOrUpdate(id, subscription, (key, oldValue) => subscription);

                        return subscribe.ToArray();
                    }
                    else
                    {
                        Console.WriteLine("ERROR [Subscribe]: Syntax error");
                        return new vhpError(1, "ERROR [Subscribe]: Syntax error").ToArray();
                    }

                case VibecodeHelpdekProtocolEnum.Disconnect:
                    if (vhpDisconnect.Parse(data) is vhpDisconnect disconnect)
                    {
                        closing = true;

                        if (Subscriptions.TryGetValue(id, out Subscriber removed))
                        {
                            Console.WriteLine($"Disconnect: {id} [{userEndPoint.Address}:{userEndPoint.Port}]");
                            CloseP2P(removed);
                        }
                        return null;
                    }
                    else
                    {
                        Console.WriteLine("ERROR [Disconnect]: Syntax error");
                        return new vhpError(9, "ERROR [Disconnect]: Syntax error").ToArray();
                    }


                case VibecodeHelpdekProtocolEnum.Redirect:
                    if (vhpRedirect.Parse(data) is vhpRedirect redirect)
                    {
                        if (Peer2Peers.TryGetValue(redirect.P2PId, out Peer2Peer p2p))
                        {
                            if (redirect.Id == p2p.Subscriber1?.Id)
                            {
                                p2p.Subscriber2.VibecodeHelpdesk.Send(redirect.Message);
                            }
                            else if (redirect.Id == p2p.Subscriber2?.Id)
                            {
                                p2p.Subscriber1.VibecodeHelpdesk.Send(redirect.Message);
                            }
                            else
                            {
                                Console.WriteLine($"ERROR [Redirect]: {redirect.Id} not found");
                                return new vhpError(4, $"ERROR [Redirect]: {redirect.Id} not found").ToArray();
                            }
                        }
                        else
                        {
                            Console.WriteLine($"ERROR [Redirect]: Peer2Peer {redirect.P2PId} not found");
                            return new vhpError(4, $"ERROR [Redirect]: Peer2Peer {redirect.P2PId} not found").ToArray();
                        }
                    }
                    else
                    {
                        Console.WriteLine("ERROR [Redirect]: Syntax error");
                        return new vhpError(4, "ERROR [Redirect]: Syntax error").ToArray();
                    }

                    return VibecodeHelpdeskProtocol_Helper.Acknowledgement;

                case VibecodeHelpdekProtocolEnum.Peer2Peer:
                    if (vhpPeer2Peer.Parse(data) is vhpPeer2Peer peer2peer)
                    {
                        if (Subscriptions.TryGetValue(peer2peer.Id, out Subscriber subscriber1))
                        {
                            if (Subscriptions.TryGetValue(peer2peer.ConnectTo, out Subscriber subscriber2))
                            {
                                var p2p = new Peer2Peer(subscriber1.Id, subscriber2.Id);

                                Console.WriteLine($"Peer2Peer {p2p.Id} [{subscriber1.Id} -> {subscriber2.Id}]");
                                Peer2Peers.AddOrUpdate(p2p.Id, p2p, (key, oldValue) => p2p);

                                peer2peer.P2PId = p2p.Id;
                                peer2peer.ServerUdpEndPoint = UDPEndPoint;

                                peer2peer.FullName = subscriber1.FullName;
                                peer2peer.Avatar = subscriber1.Avatar;

                                subscriber2.VibecodeHelpdesk.Send(peer2peer.ToArray());

                                peer2peer.FullName = subscriber2.FullName;
                                peer2peer.Avatar = subscriber2.Avatar;

                                return peer2peer.ToArray();
                            }
                            else
                            {
                                Console.WriteLine($"ERROR [Peer2Peer]: ConnectTo {peer2peer.ConnectTo} not found");
                                return new vhpError(2, $"ERROR [Peer2Peer]: ConnectTo {peer2peer.ConnectTo} not found").ToArray();
                            }
                        }
                        else
                        {
                            Console.WriteLine($"ERROR [Peer2Peer]: Id {peer2peer.Id} not found");
                            return new vhpError(2, $"ERROR [Peer2Peer]: Id {peer2peer.Id} not found").ToArray();
                        }
                    }
                    else
                    {
                        Console.WriteLine("ERROR [Peer2Peer]: Syntax error");
                        return new vhpError(2, "ERROR [Peer2Peer]: Syntax error").ToArray();
                    }
            }

            Console.WriteLine("Bad Message");
            return new byte[] { 1 };
        }
    }
}
