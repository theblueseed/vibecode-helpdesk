﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace HolePunching
{
    public class UdpSocketServer
    {
        private static bool _isRunning;
        private static UdpClient _udpClient;

        public static void Start(int port)
        {
            _udpClient = new UdpClient(port);

            Task.Run(() => ListenUdp());
        }

        public static void Stop()
        {
            _isRunning = false;

            _udpClient.Close();
            _udpClient.Dispose();
        }

        private static async void ListenUdp()
        {
            _isRunning = true;

            while (_isRunning)
            {
                try
                {
                    var receivedResults = await _udpClient.ReceiveAsync();
#pragma warning disable CS4014 // Non è possibile attendere la chiamata, pertanto l'esecuzione del metodo corrente continuerà prima del completamento della chiamata
                    Task.Run(() =>
                    {
                        if (_isRunning)
                            EngageMessage(receivedResults.Buffer, receivedResults.RemoteEndPoint);

                    });
#pragma warning restore CS4014 // Non è possibile attendere la chiamata, pertanto l'esecuzione del metodo corrente continuerà prima del completamento della chiamata
                }
                catch (Exception ex)
                {
                    if (_isRunning)
                        Console.WriteLine($"ERROR [ListenUdp]: {ex.Message}");
                }
            }
        }

        private static async void EngageMessage(byte[] data, IPEndPoint remoteEndPoint)
        {
            try
            {
                switch ((VibecodeHelpdekProtocolEnum)BitConverter.ToInt32(data, 0))
                {
                    case VibecodeHelpdekProtocolEnum.Acknowledgement:
                        Console.WriteLine($"Acknowledgement [{remoteEndPoint.Address}:{remoteEndPoint.Port}]");
                        return;

                    case VibecodeHelpdekProtocolEnum.SubscribeP2P:
                        if (vhpSubscribeP2P.Parse(data) is vhpSubscribeP2P subscribe)
                        {
                            Console.WriteLine($"SubscribeP2P: {subscribe.Id} [{remoteEndPoint.Address}:{remoteEndPoint.Port}]");

                            if (VibecodeHelpdesk.Peer2Peers.TryGetValue(subscribe.P2PId, out Peer2Peer peer2Peer))
                            {
                                if (subscribe.Id == peer2Peer.Subscriber1.Id)
                                {
                                    if (peer2Peer.Subscriber1.LocalEndPoint == null)
                                    {
                                        peer2Peer.Subscriber1.PublicEndPoint = remoteEndPoint;
                                        peer2Peer.Subscriber1.LocalEndPoint = subscribe.LocalEndPoint;
                                    }

                                    if (peer2Peer.Subscriber2.LocalEndPoint != null)
                                    {
                                        subscribe.RemoteId = peer2Peer.Subscriber2.Id;
                                        subscribe.PublicP2PEndPoint = peer2Peer.Subscriber2.PublicEndPoint;
                                        subscribe.LocalP2PEndPoint = peer2Peer.Subscriber2.LocalEndPoint;

                                        subscribe.IsLocalP2P = subscribe.PublicP2PEndPoint.Address.ToString() == remoteEndPoint.Address.ToString();
                                    }

                                    var return_data = subscribe.ToArray();
                                    await _udpClient.SendAsync(return_data, return_data.Length, remoteEndPoint);
                                }
                                else if (subscribe.Id == peer2Peer.Subscriber2.Id)
                                {
                                    if (peer2Peer.Subscriber2.LocalEndPoint == null)
                                    {
                                        peer2Peer.Subscriber2.PublicEndPoint = remoteEndPoint;
                                        peer2Peer.Subscriber2.LocalEndPoint = subscribe.LocalEndPoint;
                                    }

                                    if (peer2Peer.Subscriber1.LocalEndPoint != null)
                                    {
                                        subscribe.RemoteId = peer2Peer.Subscriber1.Id;
                                        subscribe.PublicP2PEndPoint = peer2Peer.Subscriber1.PublicEndPoint;
                                        subscribe.LocalP2PEndPoint = peer2Peer.Subscriber1.LocalEndPoint;

                                        subscribe.IsLocalP2P = subscribe.PublicP2PEndPoint.Address.ToString() == remoteEndPoint.Address.ToString();
                                    }

                                    var return_data = subscribe.ToArray();
                                    await _udpClient.SendAsync(return_data, return_data.Length, remoteEndPoint);
                                    Console.WriteLine($"SubscribeP2P Sent to {subscribe.Id}: {subscribe.RemoteId} [{remoteEndPoint.Address}:{remoteEndPoint.Port}]");
                                }
                                else
                                    Console.WriteLine($"ERROR [UDP SubscribeP2P]: {subscribe.Id} not found [{remoteEndPoint.Address}:{remoteEndPoint.Port}]");
                            }
                            else
                                Console.WriteLine($"ERROR [UDP SubscribeP2P]: Peer2Peer {subscribe.P2PId} not found [{remoteEndPoint.Address}:{remoteEndPoint.Port}]");
                        }
                        else
                            Console.WriteLine($"ERROR [UDP SubscribeP2P]: Syntax error [{remoteEndPoint.Address}:{remoteEndPoint.Port}]");
                        return;

                    case VibecodeHelpdekProtocolEnum.Redirect:
                        if (vhpRedirectUdp.Parse(data) is vhpRedirectUdp redirect)
                        {
                            Console.WriteLine($"UDP Redirect [{remoteEndPoint.Address}:{remoteEndPoint.Port}] to [{redirect.RedirectUdpEndPoint.Address}:{redirect.RedirectUdpEndPoint.Port}]");
                            await _udpClient.SendAsync(redirect.Message, redirect.Message.Length, redirect.RedirectUdpEndPoint);
                        }
                        else
                            Console.WriteLine($"ERROR [UDP Redirect]: Syntax error [{remoteEndPoint.Address}:{remoteEndPoint.Port}]");
                        return;
                }

                Console.WriteLine($"UDP bad message [{remoteEndPoint.Address}:{remoteEndPoint.Port}]");
            }
            catch(Exception e)
            {
                Console.WriteLine($"Exception [EngageMessage]: {e}");
            }
        }
    }
}
