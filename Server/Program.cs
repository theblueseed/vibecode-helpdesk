﻿//using Fleck;
using Microsoft.Extensions.Logging;
using System;
using System.Net;
using WebSocketSharper.Server;

namespace HolePunching
{
    class Program
    {
        static void Main(string[] args)
        {
            const string ip = "52.136.194.160"; // "192.168.8.102"; 

            VibecodeHelpdesk.UDPEndPoint = new IPEndPoint(IPAddress.Parse(ip), 19090);

            UdpSocketServer.Start(VibecodeHelpdesk.UDPEndPoint.Port);

            using (var loggerFactory = LoggerFactory.Create(builder => builder.AddConsole()))
            {
                ILogger logger = loggerFactory.CreateLogger<Program>();

                using (var wssv = new WebSocketServer(logger, IPAddress.Parse("0.0.0.0"), 19095))
                {
                    wssv.AddWebSocketService<VibecodeHelpdesk>("/VibecodeHelpdesk");
                    wssv.Start();
                    Console.WriteLine("Start WebSocket");

                    Console.ReadKey();

                    Console.WriteLine("Stop WebSocket");
                    try
                    {
                        wssv.Stop();
                    }
                    catch
                    { }

                    UdpSocketServer.Stop();
                }
            }
        }
    }
}
